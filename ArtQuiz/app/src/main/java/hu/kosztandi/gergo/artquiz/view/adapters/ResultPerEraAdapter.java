package hu.kosztandi.gergo.artquiz.view.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.utils.ResultData;

public class ResultPerEraAdapter extends BaseAdapter {
    private HashMap<String, Integer> source;
    private String[] keys;
    private LayoutInflater inflater;

    public ResultPerEraAdapter() {
        source = ResultData.getInstance().getHitsPerEra();

        keys = Arrays.stream(source.keySet().toArray(new String[source.size()])).filter(key -> {
            Integer value = ResultData.getInstance().getTotalPerEra().get(key);
            if (value != null) {
                return value != 0;
            } else {
                Log.e(this.getClass().getName(), "Invalid era detected! " + key);
                return false;
            }
        }).toArray(String[]::new);
    }

    @Override
    public int getCount() {
        return keys.length;
    }

    @Override
    public Object getItem(int i) {
        return source.get(keys[i]);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        String key = keys[i];

        if (inflater == null) {
            inflater = LayoutInflater.from(viewGroup.getContext());
        }

        if (view == null) {
            view = inflater.inflate(R.layout.result_per_era_row, viewGroup, false);
        }

        TextView eraNameTv = view.findViewById(R.id.eraName);
        TextView scoreTv = view.findViewById(R.id.result);

        eraNameTv.setText(key);
        scoreTv.setText(MessageFormat.format("{0} / {1}", source.get(key), ResultData.getInstance().getTotalPerEra().get(key)));

        return view;
    }
}
