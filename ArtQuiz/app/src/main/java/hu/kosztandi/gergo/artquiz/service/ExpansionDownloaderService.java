package hu.kosztandi.gergo.artquiz.service;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

public class ExpansionDownloaderService extends DownloaderService {
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApKeG+e2aIJVkDVOQFPKv7Hd6HkpniHgua5DIxsuiI/y/tH3NpNGMfhoZkSwzeek6WfJSYmkLC8yLhvqfBmqIYtGuaff1Qmip/NywaS25iIjLcXqRLqiY9Eaga/sPXwrQosIDmX36GMfwwJmUCY6vzdjMDkm28Lmwc+MhG6dsvSXx6GfElIp00V3SGtEA85i2VNviIiAjbp5t4R0fZaDcBg1FycgVfnfIlTPUwGqH5y21aOmVWUkbnEK7/TjnY+nBMs1rK7J6gZyviLLNPBYPYRa5gOm0b61A5jIhb5JVYZf3TAEghKXoQLF7azIy7vX8Juqh8dWJ+ufYsxjVoiW/lwIDAQAB";
    private static final byte[] SALT = new byte[]{ 35, 62, 17, 117, -47, 84, 65, 21, -103, -47, -12, 100, -70, -97, -22, 10, 33, -59, -120, 58 };

    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    @Override
    public byte[] getSALT() {
        return SALT;
    }

    @Override
    public String getAlarmReceiverClassName() {
        return ExpansionAlarmReceiver.class.getName();
    }
}
