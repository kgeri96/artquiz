package hu.kosztandi.gergo.artquiz.model.utilities;

public interface IObbMountedListener {
    void onObbMounted();
}
