package hu.kosztandi.gergo.artquiz.utils.quiz;

import java.util.ArrayList;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.view.fragments.MultipleSelectionFragment;
import hu.kosztandi.gergo.artquiz.view.fragments.QuizBase;
import hu.kosztandi.gergo.artquiz.view.fragments.StringInputFragment;

public class QuizFactory {
    public static QuizBase getQuizFragment(final QuizType quizType, final ArrayList<ArtPiece> artPieces) {
        QuizBase result;

        switch (quizType) {
            case NAME:
            case ERA:
                result = StringInputFragment.newInstance(artPieces, quizType);
                break;
            default:
                result = MultipleSelectionFragment.newInstance(artPieces, quizType);
        }

        return result;
    }
}