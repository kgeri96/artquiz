package hu.kosztandi.gergo.artquiz.view;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import hu.kosztandi.gergo.artquiz.R;

public class CircleProgressBar extends View {
    private static final int DURATION_QUICK_MS = 1000;
    private static final int DURATION_MEDIUM_MS = 2000;
    private static final int DURATION_SLOW_MS = 3000;

    private static final int WRONG_BOUNDARY = 40;
    private static final int CORRECT_BOUNDARY = 80;

    /**
     * ProgressBar's line thickness
     */
    private float strokeWidth = 4;
    private float progress = 0;
    private int min = 0;
    private int max = 100;
    /**
     * Start the progress at 12 o'clock
     */
    private int startAngle = -90;
    private int color = Color.DKGRAY;
    private RectF rectF;
    private Paint backgroundPaint;
    private Paint foregroundPaint;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public CircleProgressBar(Context context) {
        super(context);
    }

    public CircleProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircleProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawOval(rectF, backgroundPaint);
        float angle = 360 * progress / max;
        canvas.drawArc(rectF, startAngle, angle, false, foregroundPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);
        setMeasuredDimension(min, min);
        rectF.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2);
    }

    public void setProgress(float progress, TextView progressText) {
        ValueAnimator animator = ValueAnimator.ofFloat(0f, progress);
        animator.setDuration(getDuration(progress));
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(animation -> {
            this.progress = (float)animation.getAnimatedValue();
            setProgressText(this.progress, progressText);
            invalidate();
        });

        animateColorChange(progressText, progress);
        animator.start();
    }

    private void animateColorChange(final TextView progressText, final float progress) {
        if (progress < WRONG_BOUNDARY) {
            this.color = ContextCompat.getColor(progressText.getContext(), R.color.wrongFeedbackResultColor);
            progressText.setTextColor(this.color);
            setColor();
            invalidate();
        }
        else if (progress >= WRONG_BOUNDARY && progress < CORRECT_BOUNDARY) {
            ValueAnimator wrongToInaccurate = createAnimator(progressText, ContextCompat.getColor(progressText.getContext(), R.color.wrongFeedbackResultColor),
                    ContextCompat.getColor(progressText.getContext(), R.color.inaccurateFeedbackResultColor), getDuration(progress));

            wrongToInaccurate.start();
        } else {
            ValueAnimator wrongToInaccurate = createAnimator(progressText, ContextCompat.getColor(progressText.getContext(), R.color.wrongFeedbackResultColor),
                    ContextCompat.getColor(progressText.getContext(), R.color.inaccurateFeedbackResultColor), getDuration(progress) / 2);
            ValueAnimator inaccurateToCorrect = createAnimator(progressText, ContextCompat.getColor(progressText.getContext(), R.color.inaccurateFeedbackResultColor),
                    ContextCompat.getColor(progressText.getContext(), R.color.correctFeedbackResultColor), getDuration(progress) / 2);

            wrongToInaccurate.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    inaccurateToCorrect.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            wrongToInaccurate.start();
        }
    }

    private ValueAnimator createAnimator(TextView progressText, int fromColor, int toColor, int duration) {
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), fromColor, toColor);
        colorAnimation.setDuration(duration);
        colorAnimation.addUpdateListener(animator -> {
            progressText.setTextColor((int) animator.getAnimatedValue());
            this.color = (int) animator.getAnimatedValue();
            setColor();
            invalidate();
        });

        return colorAnimation;
    }

    private int getDuration(float progress) {
        if (progress < WRONG_BOUNDARY) {
            return DURATION_QUICK_MS;
        } else if (progress >= WRONG_BOUNDARY && progress < CORRECT_BOUNDARY) {
            return DURATION_MEDIUM_MS;
        } else {
            return DURATION_SLOW_MS;
        }
    }

    private void setProgressText(float progress, TextView progressText) {
        progressText.setText(MessageFormat.format("{0}%", decimalFormat.format(progress)));
    }

    private void init(Context context, AttributeSet attrs) {
        rectF = new RectF();
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CircleProgressBar,
                0, 0);
        //Reading values from the XML layout
        try {
            strokeWidth = typedArray.getDimension(R.styleable.CircleProgressBar_progressBarThickness, strokeWidth);
            progress = typedArray.getFloat(R.styleable.CircleProgressBar_progress, progress);
            color = typedArray.getInt(R.styleable.CircleProgressBar_progressbarColor, color);
            min = typedArray.getInt(R.styleable.CircleProgressBar_min, min);
            max = typedArray.getInt(R.styleable.CircleProgressBar_max, max);
        } finally {
            typedArray.recycle();
        }

        setColor();
    }

    private int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    private void setColor() {
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(adjustAlpha(color, 0.3f));
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setStrokeWidth(strokeWidth);

        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(color);
        foregroundPaint.setStyle(Paint.Style.STROKE);
        foregroundPaint.setStrokeWidth(strokeWidth);
    }
}
