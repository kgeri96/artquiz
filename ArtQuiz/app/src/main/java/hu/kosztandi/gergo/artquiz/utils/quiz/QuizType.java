package hu.kosztandi.gergo.artquiz.utils.quiz;

public enum QuizType {
    NAME, MULTIPLE_SELECTION, ERA
}