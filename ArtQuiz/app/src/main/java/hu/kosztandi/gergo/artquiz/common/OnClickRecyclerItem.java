package hu.kosztandi.gergo.artquiz.common;

public interface OnClickRecyclerItem {
    void onClick(int pos);
}
