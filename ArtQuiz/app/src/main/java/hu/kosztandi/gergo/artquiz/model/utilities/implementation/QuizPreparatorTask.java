package hu.kosztandi.gergo.artquiz.model.utilities.implementation;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import hu.kosztandi.gergo.artquiz.common.BackgroundTask;
import hu.kosztandi.gergo.artquiz.view.dialogs.LoadDialogHandler;

public class QuizPreparatorTask extends AsyncTask<Void, Void, Void> {
    private BackgroundTask listener;
    private Dialog dialog;

    public QuizPreparatorTask(BackgroundTask listener, Context context) {
        this.listener = listener;
        dialog = LoadDialogHandler.getLoadDialog(context);
        dialog.show();
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        listener.runTask();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onFinish();
        dialog.dismiss();
    }
}
