package hu.kosztandi.gergo.artquiz.common;

public interface BottomBarChangeListener {
    void switchVisibility();
}
