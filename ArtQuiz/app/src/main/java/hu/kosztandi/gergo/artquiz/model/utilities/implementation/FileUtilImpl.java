package hu.kosztandi.gergo.artquiz.model.utilities.implementation;

import android.content.Context;
import android.os.storage.OnObbStateChangeListener;
import android.os.storage.StorageManager;
import android.util.Log;

import java.io.File;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import hu.kosztandi.gergo.artquiz.BuildConfig;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.model.utilities.IObbMountedListener;

public class FileUtilImpl implements FileUtil {
    private static final String FILE_NAME = MessageFormat.format("main.{0}.hu.kosztandi.gergo.artquiz.obb", BuildConfig.OBB_VERSION);

    private final String FILE_PATH;
    private final StorageManager storageManager;
    private final IObbMountedListener listener;

    private String mountedObbPath;

    public FileUtilImpl(final Context context, IObbMountedListener listener) {
        this.listener = listener;
        FILE_PATH = String.format("%s/%s", context.getObbDir(), FILE_NAME);
        storageManager = (StorageManager)context.getSystemService(Context.STORAGE_SERVICE);
        mountObb();
    }

    private void mountObb() {
        if (storageManager != null && checkExpansionFile() && !storageManager.isObbMounted(FILE_PATH)) {
            storageManager.mountObb(FILE_PATH, null, new OnObbStateChangeListener() {
                @Override
                public void onObbStateChange(final String path, final int state) {
                    super.onObbStateChange(path, state);
                    if (state == ERROR_ALREADY_MOUNTED) {
                        return;
                    }

                    if (state != MOUNTED) {
                        throw new RuntimeException("Obb could not be mounted");
                    }

                    mountedObbPath = storageManager.getMountedObbPath(FILE_PATH);
                    listener.onObbMounted();
                }
            });
        } else {
            throw new RuntimeException("Storage Service is not available or expansion was not found!");
        }
    }

    @Override
    public boolean checkExpansionFile() {
        return new File(FILE_PATH).exists();
    }

    @Override
    public final File getImageByName(final String fileName) {
        AtomicReference<File> result = new AtomicReference<>(null);

        if (mountedObbPath == null || mountedObbPath.isEmpty()) {
            throw new RuntimeException("Obb yet to be mounted!");
        }

        final String path = String.format("%s/%s", mountedObbPath, fileName);

        Optional.ofNullable(new File(path).listFiles())
                .ifPresent(files -> {
                    Optional<File> match = Arrays.stream(files).filter(file -> file.getName().contains(fileName)).findFirst();
                    if (match.isPresent()) {
                        result.set(match.get());
                    } else {
                        Log.e("FileNotFound", MessageFormat.format("File with name: {0} \ncould not be found in directory: {1}!", fileName, path));
                    }
                });

        return result.get();
    }
}
