package hu.kosztandi.gergo.artquiz.view.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.MessageFormat;

import hu.kosztandi.gergo.artquiz.BuildConfig;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;

public class MainActivity extends AppCompatActivity {
    private final String[] toastMessages = {"Alkalmazást készítették:", "Képválogatás:\n Csókási Katalin Nóra", "Tervezés és képek bevitele:\nTápai Melinda", "Programozás és tervezés:\nKosztándi Gergő"};
    private final int[] drawableIds = {R.drawable.ic_icon_round, R.drawable.k_icon, R.drawable.m_icon, R.drawable.g_icon};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        AppUtilities.loadImageToContainer(findViewById(R.id.container), R.drawable.bg_main_menu_2);
        init();
    }

    private void init() {
        QuizSettings.getInstance().init(this);
        setReferences();
    }

    private void setReferences() {
        setStartQuizButton();
        setSettingsButton();
        setAboutButton();
        setHandleImagesButton();
        setVersionCode();
    }

    private void setVersionCode() {
        TextView textView = findViewById(R.id.versionName);
        textView.setText(MessageFormat.format("v1.{0}", BuildConfig.VERSION_CODE - 1));
    }

    private void setStartQuizButton() {
        Button startQuizButton = findViewById(R.id.startQuizButton);
        startQuizButton.setOnClickListener(view -> {
            final Intent intent = new Intent(MainActivity.this, QuizSettings.getInstance().isLearnMode() ? LearnModeActivity.class : QuizActivity.class);
            startActivity(intent);
        });
    }

    private void setSettingsButton() {
        Button settingsButton = findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        });
    }

    private void setHandleImagesButton() {
        Button handleImagesButton = findViewById(R.id.handleImagesButton);
        handleImagesButton.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, ImageHandlerActivity.class);
            startActivity(intent);
        });
    }

    private void setAboutButton() {
        Button aboutButton = findViewById(R.id.aboutButton);
        aboutButton.setOnClickListener(view -> showCredits());
    }

    private void showCredits() {
        final Handler handler = new Handler();
        for(int i = 0; i < toastMessages.length; i++) {
            handler.postDelayed((new ToastRunnable(toastMessages[i], drawableIds[i])), i*3750);
        }
    }

    private class ToastRunnable implements Runnable {
        private String msg;
        private int drawableId;
        ToastRunnable(String msg, int drawableId) {
            this.msg = msg;
            this.drawableId = drawableId;
        }
        @Override
        public void run() {
            LayoutInflater inflater = getLayoutInflater();
            @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.custom_toast, null);

            ImageView icon = view.findViewById(R.id.nameIcon);
            TextView text = view.findViewById(R.id.message);

            text.setText(msg);

            icon.setImageDrawable(getDrawable(drawableId));

            Toast toast = new Toast(MainActivity.this);
            toast.setView(view);
            toast.show();
        }
    }
}
