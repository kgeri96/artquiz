package hu.kosztandi.gergo.artquiz.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import java.util.Objects;
import hu.kosztandi.gergo.artquiz.R;

public class LoadDialogHandler {

    public static Dialog getLoadDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.load_dialog);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }
}