package hu.kosztandi.gergo.artquiz.utils.quiz;

public enum UserAction {
    EVALUATE,
    NEXT,
    FINISH,
    RETRY,
    EXIT
}
