package hu.kosztandi.gergo.artquiz.model.settings;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

import hu.kosztandi.gergo.artquiz.ArtQuiz;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.utils.quiz.QuizType;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;

import static android.content.Context.MODE_PRIVATE;

public class QuizSettings {
    private static final QuizSettings ourInstance = new QuizSettings();
    private boolean isLearnMode;
    private boolean isNotFullMatchOnly;
    private QuizType quizType;
    private ArrayList<String> selectedEras;
    private long questionLimit;
    private boolean isSoundOn;

    public static QuizSettings getInstance() {
        return ourInstance;
    }

    private QuizSettings() {

    }

    public void savePrefs() {
        SharedPreferences pref = ArtQuiz.getContext().getSharedPreferences("QuizSettings", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isLearnMode", isLearnMode);
        editor.putLong("questionLimit", questionLimit);
        editor.putString("quizType", quizType.name());
        editor.putStringSet("selectedEras", new HashSet<>(selectedEras));
        editor.putBoolean("isNotFullMatchOnly", isNotFullMatchOnly);
        editor.putBoolean("isSoundOn", isSoundOn);
        editor.apply();
    }

    public void init(Context context) {
        SharedPreferences pref = context.getSharedPreferences("QuizSettings", MODE_PRIVATE);
        if(!pref.contains("selectedEras")) {
            selectedEras = new ArrayList<>();
            selectedEras.add(context.getResources().getString(R.string.selection_all));
            long artPieceCount = ArtPiecesService.getInstance().getArtPieceCount();
            if(artPieceCount < 20) {
                questionLimit = artPieceCount;
            } else {
                questionLimit = 20;
            }
            quizType = QuizType.NAME;
            isLearnMode = false;
            isNotFullMatchOnly = true;
            isSoundOn = true;
        } else {
            selectedEras = new ArrayList<>(Objects.requireNonNull(pref.getStringSet("selectedEras", null)));
            isLearnMode = pref.getBoolean("isLearnMode", false);
            questionLimit = pref.getLong("questionLimit", 1);
            isNotFullMatchOnly = pref.getBoolean("isNotFullMatchOnly", true);
            String qt = pref.getString("quizType", "");
            switch (qt) {
                case "NAME":
                    quizType = QuizType.NAME;
                    break;
                case "ERA":
                    quizType = QuizType.ERA;
                    break;
                case "MULTIPLE_SELECTION":
                    quizType = QuizType.MULTIPLE_SELECTION;
                    break;
                default:
                    quizType = QuizType.NAME;
                    break;
            }
            isSoundOn = pref.getBoolean("isSoundOn", true);
        }
    }

    public boolean isLearnMode() {
        return isLearnMode;
    }

    public void setLearnMode(boolean isLearnMode) {
        this.isLearnMode = isLearnMode;
    }

    public QuizType getQuizType() {
        return quizType;
    }

    public void setQuizType(QuizType quizType) {
        this.quizType = quizType;
    }

    public ArrayList<String> getSelectedEras() {
        return selectedEras;
    }

    private void removeSelectedEra(String era) {
        selectedEras.remove(era);
    }

    public void setSelectedEras(ArrayList<String> selectedEras) {
        this.selectedEras = selectedEras;
    }

    public long getQuestionLimit() {
        return questionLimit;
    }

    public void setQuestionLimit(long questionLimit) {
        this.questionLimit = questionLimit;
    }

    public boolean isNotFullMatchOnly() {
        return isNotFullMatchOnly;
    }

    public void setNotFullMatchOnly(boolean notFullMatchOnly) {
        isNotFullMatchOnly = notFullMatchOnly;
    }

    public void updateSelectedErasAndLimit(String era) {
        if(selectedEras.contains(era)) {
            ArrayList<String> countOfArtPiecesInRemovedEra = new ArrayList<>();
            countOfArtPiecesInRemovedEra.add(era);
            long count = ArtPiecesService.getInstance().getArtPieceCount(countOfArtPiecesInRemovedEra);
            long selectedCount = ArtPiecesService.getInstance().getArtPieceCount(selectedEras);

            if(questionLimit == selectedCount) {
                questionLimit--;
            }

            if(count == 0) {
                removeSelectedEra(era);
            }
            savePrefs();
        }
    }

    public boolean isSoundOn() {
        return isSoundOn;
    }

    public void setSoundOn(boolean soundOn) {
        isSoundOn = soundOn;
    }
}
