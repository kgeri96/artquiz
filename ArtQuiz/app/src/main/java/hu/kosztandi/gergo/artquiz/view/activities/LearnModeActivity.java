package hu.kosztandi.gergo.artquiz.view.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import java.text.MessageFormat;
import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.BackgroundTask;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.CustomViewPager;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.FileUtilImpl;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.QuizPreparatorTask;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;
import hu.kosztandi.gergo.artquiz.view.adapters.ArtPiecePagerAdapter;

public class LearnModeActivity extends AppCompatActivity {
    private CustomViewPager viewPager;
    private ArrayList<ArtPiece> artPieces;
    private ArtPiecePagerAdapter adapter;
    private ViewGroup bottomBar;
    private TextView artPieceTitle;
    private TextView artPieceEra;
    private Toast statusToast;
    private FileUtil fileUtil;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_mode);

        viewPager = findViewById(R.id.artPiecePager);
        artPieceTitle = findViewById(R.id.artPieceTitle);
        artPieceEra = findViewById(R.id.artPieceEra);
        bottomBar = findViewById(R.id.bottomBar);
        statusToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        new QuizPreparatorTask(new BackgroundTask() {
            @Override
            public void runTask() {
                artPieces = ArtPiecesService.getInstance().getArtPiecesForSelectedEras();
                if (artPieces.size() < 1 || QuizSettings.getInstance().getQuestionLimit() < 1) {
                    LearnModeActivity.this.finish();
                }

                fileUtil = new FileUtilImpl(LearnModeActivity.this, () -> {
                    adapter = new ArtPiecePagerAdapter(LearnModeActivity.this, artPieces, LearnModeActivity.this::switchBarVisibility, fileUtil);
                    viewPager.setAdapter(adapter);
                });
            }

            @Override
            public void onFinish() {
                updateBottomBarAndPopToast(0);
                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {

                    }

                    @Override
                    public void onPageSelected(int i) {
                        updateBottomBarAndPopToast(i);
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });
            }
        }, this).execute();
    }

    private void updateBottomBarAndPopToast(int i) {
        String status = MessageFormat.format("{0} / {1}",i + 1, artPieces.size());
        statusToast.setText(status);
        statusToast.show();
        artPieceTitle.setText(artPieces.get(i).getName());
        artPieceEra.setText(artPieces.get(i).getEra());
    }

    private void switchBarVisibility() {
        if(bottomBar.getVisibility() == View.VISIBLE) {
            bottomBar.setVisibility(View.INVISIBLE);
            bottomBar.invalidate();
        } else {
            bottomBar.setVisibility(View.VISIBLE);
            bottomBar.invalidate();
        }
    }
}
