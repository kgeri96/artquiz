package hu.kosztandi.gergo.artquiz.view.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.Evaluator;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResponse;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResult;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResultFactory;
import hu.kosztandi.gergo.artquiz.utils.quiz.QuizType;
import hu.kosztandi.gergo.artquiz.view.FeedbackEditText;

public class StringInputFragment extends ImageQuiz {
    private FeedbackEditText answerBox;
    private ImageButton doneButton;
    private Activity context;
    private ViewGroup imageFragmentContainer;
    private TextView correctAnswer;
    private Animation slideAnimation;
    private Animation slideOutAnimation;

    public static QuizBase newInstance(final ArrayList<ArtPiece> artPieces, final QuizType quizType) {
        StringInputFragment fragment = new StringInputFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(ART_PIECE_LIST_KEY, artPieces);
        args.putInt(QUIZ_TYPE_KEY, quizType.ordinal());
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();

        final View view = inflater.inflate(R.layout.fragment_string_input_quiz, container, false);

        doneButton = view.findViewById(R.id.doneButton);
        answerBox = view.findViewById(R.id.answerBox);
        imageFragmentContainer = view.findViewById(R.id.imageContainer);
        correctAnswer = view.findViewById(R.id.correctAnswer);

        correctAnswer.setVisibility(View.GONE);

        setAnswerBox();
        setDoneButton();
        setSlideAnimation();

        return view;
    }

    @Override
    protected void updateUI() {
        imageFragment.updateImage(artPieces.get(currentArtPieceIndex).getImageUrl());
    }

    @Override
    protected void handleFeedback(final EvaluationResult result) {
        answerBox.feedback(result.response);

        if (result.response != EvaluationResponse.CORRECT) {
            correctAnswer.setText(result.message);
            correctAnswer.setVisibility(View.VISIBLE);
            correctAnswer.startAnimation(slideAnimation);
        }
    }

    @Override
    protected Evaluator getQuizEvaluator() {
        final String correctAnswer = quizType == QuizType.ERA ? artPieces.get(currentArtPieceIndex).getEra() : artPieces.get(currentArtPieceIndex).getName();
        return userAnswer -> EvaluationResultFactory.create(quizType, userAnswer, correctAnswer);
    }

    @Override
    protected ViewGroup getImageFragmentContainer() {
        return imageFragmentContainer;
    }

    @Override
    public int getCorrectAnswerValue() {
        return 2;
    }

    private void setSlideAnimation() {
        slideAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_anim_bottom);
        slideAnimation.setFillAfter(true);
        slideAnimation.setInterpolator(new AccelerateInterpolator());
        slideOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_anim_bottom);
        slideOutAnimation.setInterpolator(new AccelerateInterpolator());
        slideOutAnimation.setFillAfter(true);
    }

    private void setDoneButton() {
        doneButton.setOnClickListener(this::onDoneAction);
    }

    private void clearAnswerField() {
        if (answerBox.getText() != null) {
            answerBox.getText().clear();
        }
        answerBox.reset();
        answerBox.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    private void setAnswerBox() {
        answerBox.setOnClickListener(view -> answerBox.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS));
        setHint();
    }

    private void setAnswerBox(boolean reset) {
        if (reset) {
            answerBox.setKeyListener((KeyListener) answerBox.getTag());
            answerBox.setTextColor(context.getColor(R.color.menuTextColor));
        } else {
            answerBox.setTag(answerBox.getKeyListener());
            answerBox.setKeyListener(null);
        }
    }

    private void setHint() {
        switch (quizType) {
            case NAME:
                answerBox.setHint(context.getString(R.string.edit_text_hint_name));
                break;

            case ERA:
                answerBox.setHint(context.getString(R.string.edit_text_hint_era));
                break;
        }
    }

    private void onDoneAction(View view) {
        if (view.isSelected()) {
            setAnswerBox(true);
            view.setSelected(false);
            clearAnswerField();
            answerBox.setEnabled(true);
            correctAnswer.startAnimation(slideOutAnimation);
            next();
        } else {
            AppUtilities.getInstance().hideKeyboard((AppCompatActivity) context);
            evaluate(answerBox.getText() != null ? answerBox.getText().toString() : "");
            answerBox.setEnabled(false);
            setAnswerBox(false);
            view.setSelected(true);
        }
    }
}
