package hu.kosztandi.gergo.artquiz.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import hu.kosztandi.gergo.artquiz.common.Evaluator;
import hu.kosztandi.gergo.artquiz.common.FeedbackListener;
import hu.kosztandi.gergo.artquiz.common.QuizActionHandler;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.utils.ResultData;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResult;
import hu.kosztandi.gergo.artquiz.utils.quiz.QuizType;
import hu.kosztandi.gergo.artquiz.utils.quiz.UserAction;

public abstract class QuizBase extends Fragment {
    static final String ART_PIECE_LIST_KEY = "artPieces";
    static final String QUIZ_TYPE_KEY = "quizType";

    ArrayList<ArtPiece> artPieces;
    QuizType quizType;

    int currentArtPieceIndex = 0;

    private Evaluator evaluator;
    private QuizActionHandler actionHandler;
    private FeedbackListener feedbackListener;

    public QuizBase() {

    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            artPieces = getArguments().getParcelableArrayList(ART_PIECE_LIST_KEY);
            if (artPieces == null || artPieces.isEmpty()) {
                dispatchAction(UserAction.EXIT);
                Log.e("FRAGMENT_ERROR", "ArtPieces were not put in bundle");
            } else {
                Collections.shuffle(artPieces);
                ResultData.getInstance().maxScore = getCorrectAnswerValue() * artPieces.size();
            }

            quizType = QuizType.values()[getArguments().getInt(QUIZ_TYPE_KEY)];
        }

        evaluator = getQuizEvaluator();
    }

    @Override
    public abstract View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                                      Bundle savedInstanceState);

    @Override
    public void onAttach(@NonNull final Context context) {
        super.onAttach(context);
        if (context instanceof QuizActionHandler && context instanceof FeedbackListener) {
            actionHandler = (QuizActionHandler) context;
            feedbackListener = (FeedbackListener) context;
        } else {
            throw new RuntimeException(MessageFormat.format("{0} must implement: {1}, {2}", context.toString(),
                    QuizActionHandler.class.getSimpleName(), FeedbackListener.class.getSimpleName()));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        actionHandler = null;
        feedbackListener = null;
    }

    public void handleRetry() {
        currentArtPieceIndex = 0;
        Collections.shuffle(artPieces);
        dispatchAction(UserAction.RETRY);
        updateUI();
    }

    public ArtPiece getCurrentArtPiece() {
        return artPieces.get(currentArtPieceIndex);
    }

    public int getCurrentArtPieceIndex() {
        return currentArtPieceIndex;
    }

    public abstract int getCorrectAnswerValue();
    protected abstract void updateUI();
    protected abstract void handleFeedback(final EvaluationResult result);
    protected abstract Evaluator getQuizEvaluator();
    protected abstract ViewGroup getImageFragmentContainer();

    void evaluate(final String answer) {
        final EvaluationResult result = evaluator.evaluate(answer);
        dispatchAction(UserAction.EVALUATE);

        switch (result.response) {
            case CORRECT:
                feedbackListener.onCorrectAnswer();
                handleFeedback(result);
                break;
            case INCORRECT:
                feedbackListener.onWrongAnswer();
                handleFeedback(result);
                break;
            case PARTLY_CORRECT:
                feedbackListener.onInaccurateAnswer(result.message);
                handleFeedback(result);
                break;
        }
    }

    final void next() {
        if (currentArtPieceIndex == artPieces.size() - 1) {
            dispatchAction(UserAction.FINISH);
        } else {
            currentArtPieceIndex++;
            dispatchAction(UserAction.NEXT);
            updateUI();
        }
    }

    private void dispatchAction(final UserAction action) {
        actionHandler.onUserAction(action);
    }
}