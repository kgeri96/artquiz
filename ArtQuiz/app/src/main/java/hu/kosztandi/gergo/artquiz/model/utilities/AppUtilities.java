package hu.kosztandi.gergo.artquiz.model.utilities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.request.transition.ViewPropertyTransition;
import java.io.File;
import hu.kosztandi.gergo.artquiz.GlideApp;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.common.PermissionCommand;

public class AppUtilities {
    private static final AppUtilities ourInstance = new AppUtilities();
    private static final int ROUNDNESS = 20;

    public static AppUtilities getInstance() {
        return ourInstance;
    }

    private AppUtilities() {
    }

    public void hideKeyboard(final AppCompatActivity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void loadImage(final Context context, final FileUtil fileUtil, final ImageView imageView, final String name, final boolean isRounded) {
        final ViewPropertyTransition.Animator animationObject = getAnimator();
        final File file = new File(name);
        final File image = file.exists() && isImage(file) ? file : fileUtil.getImageByName(name);

        if (image != null) {
            loadImage(context, imageView, animationObject, image, isRounded);
        } else {
            loadImage(context, imageView, animationObject, Uri.parse(name), isRounded);
        }
    }

    private static void loadImage(final Context context, final ImageView imageView, final ViewPropertyTransition.Animator animationObject, final Uri uri,
                                  final boolean isRounded) {
        GlideApp.with(imageView.getContext())
                .load(uri)
                .apply(new RequestOptions().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE))
                .placeholder(new ColorDrawable(context.getColor(R.color.placeHolder)))
                .transform(new RoundedCorners(isRounded ? ROUNDNESS : 1))
                .transition(GenericTransitionOptions.with(animationObject))
                .error(R.drawable.ic_alert)
                .into(imageView);
    }

    private static void loadImage(final Context context, final ImageView imageView, final ViewPropertyTransition.Animator animationObject, final File image,
                                  final boolean isRounded) {
        GlideApp.with(imageView.getContext())
                .load(image)
                .apply(new RequestOptions().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE))
                .placeholder(new ColorDrawable(context.getColor(R.color.placeHolder)))
                .transform(new RoundedCorners(isRounded ? ROUNDNESS : 1))
                .transition(GenericTransitionOptions.with(animationObject))
                .error(R.drawable.ic_alert)
                .into(imageView);
    }

    private static ViewPropertyTransition.Animator getAnimator() {
        return view -> {
            view.setAlpha(0f);
            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(500);
            fadeAnim.start();
        };
    }

    private static boolean isImage(final File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

        return bitmap != null;
    }

    public static void askForPermissionAndExecuteCommand(final AppCompatActivity context, final int requestCode, final String permission,
                                                         final int PermissionRequestTextResource, final PermissionCommand command) {

        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
                Toast.makeText(context, PermissionRequestTextResource, Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(context, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(context,
                        new String[]{permission},
                        requestCode);
            }
        } else {
            command.execute();
        }
    }

    public static void loadImageToContainer(final ViewGroup container, final int resourceId) {
        GlideApp.with(container.getContext())
                .load(resourceId)
                .placeholder(new ColorDrawable(ContextCompat.getColor(container.getContext(), R.color.grayLight)))
                .into(new CustomTarget<Drawable>() {

            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                container.setBackground(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                container.setBackground(placeholder);
            }
        });
    }
}
