package hu.kosztandi.gergo.artquiz.view.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.qtalk.recyclerviewfastscroller.RecyclerViewFastScroller;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;
import hu.kosztandi.gergo.artquiz.common.OnClickRecyclerItem;

public class ImageHandlerAdapter extends RecyclerView.Adapter<ImageHandlerAdapter.ViewHolder> implements RecyclerViewFastScroller.OnPopupTextUpdate {

    private ArrayList<ArtPiece> artPieces;
    private OnClickRecyclerItem listener;
    private int lastPosition = -1;
    private FileUtil fileUtil;

    public ImageHandlerAdapter(ArrayList<ArtPiece> artPieces, OnClickRecyclerItem listener, FileUtil fileUtil) {
        this.artPieces = artPieces;
        this.listener = listener;
        this.fileUtil = fileUtil;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.art_piece_row_recycler, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final @NonNull ViewHolder viewHolder, final int pos) {
        AppUtilities.loadImage(viewHolder.itemView.getContext(), fileUtil, viewHolder.artPieceImage, artPieces.get(pos).getImageUrl(), true);
        viewHolder.eraText.setText(artPieces.get(pos).getEra());
        viewHolder.nameText.setText(artPieces.get(pos).getName());
        viewHolder.deleteButton.setOnClickListener(view -> listener.onClick(pos));

        viewHolder.artPieceImage.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(view.getContext());
            dialog.setContentView(R.layout.view_image_dialog);
            AppUtilities.loadImage(view.getContext(), fileUtil, dialog.findViewById(R.id.image), artPieces.get(pos).getImageUrl(), false);
            dialog.findViewById(R.id.cancelButton).setOnClickListener(button -> dialog.cancel());

            dialog.show();
        });

        setAnimation(viewHolder.itemView, pos);
    }

    private void setAnimation(final View itemView, final int position) {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(itemView.getContext(), R.anim.fall_in_anim);
            itemView.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return artPieces.size();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        holder.clearAnimation();
    }

    @NotNull
    @Override
    public CharSequence onChange(int i) {
        return artPieces.get(i).getEra();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView artPieceImage;
        TextView eraText;
        TextView nameText;
        ImageButton deleteButton;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            artPieceImage = itemView.findViewById(R.id.artPieceImage);
            eraText = itemView.findViewById(R.id.eraText);
            nameText = itemView.findViewById(R.id.nameText);
            deleteButton = itemView.findViewById(R.id.removeButton);
        }

        void clearAnimation()
        {
            itemView.clearAnimation();
        }
    }
}
