package hu.kosztandi.gergo.artquiz.model;

public final class AppConstants {
    //public static final String assetDirectory = "file:///android_asset/images/";
    public static final int multipleSelectionNumber = 4;
    public static final int answerThreshold = 4;
    public static final int DATABASE_VERSION = 1;

    public static final float StringInputAccuracyThreshold = 0.85f;
    public static final int SCORE_NAME = 2;
    public static final int SCORE_NAME_INACCURATE_ANSWER = 1;
    public static final int SCORE_SIMPLE = 1;
}