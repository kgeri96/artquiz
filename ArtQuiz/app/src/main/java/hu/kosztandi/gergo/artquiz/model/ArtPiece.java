package hu.kosztandi.gergo.artquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ArtPiece implements Parcelable {
    private String name;
    private String era;
    private String imageUrl;
    private int id;

    public ArtPiece(final int id, final String name, final String era, final String imageUrl) {
        this.id = id;
        this.name = name;
        this.era = era;
        this.imageUrl = imageUrl;
    }

    public ArtPiece(final String name, final String era, final String imageUrl) {
        this.name = name;
        this.era = era;
        this.imageUrl = imageUrl;
    }

    private ArtPiece(Parcel in) {
        name = in.readString();
        era = in.readString();
        imageUrl = in.readString();
        id = in.readInt();
    }

    public static final Creator<ArtPiece> CREATOR = new Creator<ArtPiece>() {

        @Override
        public ArtPiece createFromParcel(Parcel in) {
            return new ArtPiece(in);
        }

        @Override
        public ArtPiece[] newArray(int size) {
            return new ArtPiece[size];
        }
    };

    public final int getId() {
        return id;
    }

    public final String getName() {
        return name;
    }

    public final String getEra() {
        return era;
    }

    public final String getImageUrl() {
        return imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(era);
        parcel.writeString(imageUrl);
        parcel.writeInt(id);
    }
}
