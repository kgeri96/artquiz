package hu.kosztandi.gergo.artquiz.common;

public interface ResultClickHandler {
    void onClickDone();
    void onClickRetry();
}
