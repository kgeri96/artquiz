package hu.kosztandi.gergo.artquiz.common;

import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResult;

public interface Evaluator {
    EvaluationResult evaluate(String userAnswer);
}
