package hu.kosztandi.gergo.artquiz.view.activities;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.BackgroundTask;
import hu.kosztandi.gergo.artquiz.common.FeedbackListener;
import hu.kosztandi.gergo.artquiz.common.QuizActionHandler;
import hu.kosztandi.gergo.artquiz.common.ResultClickHandler;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.QuizPreparatorTask;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResponse;
import hu.kosztandi.gergo.artquiz.utils.quiz.QuizFactory;
import hu.kosztandi.gergo.artquiz.utils.quiz.UserAction;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;
import hu.kosztandi.gergo.artquiz.utils.ResultData;
import hu.kosztandi.gergo.artquiz.view.dialogs.ResultDialogHandler;
import hu.kosztandi.gergo.artquiz.view.fragments.QuizBase;

public class QuizActivity extends AppCompatActivity implements FeedbackListener, QuizActionHandler {
    private static final int FADE_FEEDBACK_DURATION_MS = 150;
    private ArrayList<ArtPiece> artPieceList;

    private ViewGroup statusBar;
    private TextView scoreTextView;
    private TextView status;
    private QuizBase quizBase;
    private ViewGroup quizContainer;
    private Animation slideAnimation;
    private Animation slideOutAnimation;
    private Animation fadeInAnimation;
    private Animation fadeOutAnimation;
    private View flashFeedbackView;

    private MediaPlayer correctSoundPlayer;
    private MediaPlayer wrongSoundPlayer;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        init();
    }

    @Override
    public void onCorrectAnswer() {
        ResultData.getInstance().addHit(quizBase.getCurrentArtPiece().getEra(), quizBase.getCorrectAnswerValue(), quizBase.getCorrectAnswerValue());
        playFlashFeedback(EvaluationResponse.CORRECT);
        playSoundFeedback(true);
        updateScore();
    }

    @Override
    public void onInaccurateAnswer(final String feedback) {
        ResultData.getInstance().addHit(quizBase.getCurrentArtPiece().getEra(),  quizBase.getCorrectAnswerValue() / 2, quizBase.getCorrectAnswerValue());
        playFlashFeedback(EvaluationResponse.PARTLY_CORRECT);
        playSoundFeedback(true);
        updateScore();
    }

    @Override
    public void onWrongAnswer() {
        ResultData.getInstance().addMiss(quizBase.getCurrentArtPiece().getEra(), quizBase.getCorrectAnswerValue());
        playFlashFeedback(EvaluationResponse.INCORRECT);
        playSoundFeedback(false);
    }

    private void init() {
        scoreTextView = findViewById(R.id.score);
        status = findViewById(R.id.status);
        quizContainer = findViewById(R.id.quizContainer);
        statusBar = findViewById(R.id.statusBar);
        flashFeedbackView = findViewById(R.id.flashFeedback);
        ResultData.getInstance().clearScore();

        setSlideAnimation();
        fadeInAnimation = createFadeAnimation(true);
        fadeOutAnimation = createFadeAnimation(false);
        setFlashFeedbackAnimation();

        statusBar.setVisibility(View.GONE);

        new QuizPreparatorTask(new BackgroundTask() {

            @Override
            public void runTask() {
                artPieceList = ArtPiecesService.getInstance().getConfiguredShuffledArtPieceList();
                setupMediaPlayer();
            }

            @Override
            public void onFinish() {
                if (QuizSettings.getInstance().getQuestionLimit() == 0 || QuizSettings.getInstance().getSelectedEras().size() == 0 || artPieceList.size() < 1) {
                    Toast.makeText(QuizActivity.this, R.string.warning_no_art_selected, Toast.LENGTH_LONG).show();
                    finish();
                }

                setQuizFragment();
                updateStatus();
                updateScore();
            }
        }, this).execute();
    }

    private void playFlashFeedback(final EvaluationResponse result) {
        switch (result) {
            case CORRECT:
                flashFeedbackView.setBackgroundColor(ContextCompat.getColor(this, R.color.correctFeedbackColor));
                break;
            case INCORRECT:
                flashFeedbackView.setBackgroundColor(ContextCompat.getColor(this, R.color.wrongFeedbackColor));
                break;
            case PARTLY_CORRECT:
                flashFeedbackView.setBackgroundColor(ContextCompat.getColor(this, R.color.inaccurateFeedbackColor));
                break;
        }

        flashFeedbackView.startAnimation(fadeInAnimation);
    }

    private void setSlideAnimation() {
        slideAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_anim);
        slideAnimation.setFillAfter(true);
        slideAnimation.setInterpolator(new AccelerateInterpolator());
        slideOutAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_out_anim);
        slideOutAnimation.setInterpolator(new AccelerateInterpolator());
        slideOutAnimation.setFillAfter(true);
    }

    private void setFlashFeedbackAnimation() {
        fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                flashFeedbackView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                flashFeedbackView.startAnimation(fadeOutAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                flashFeedbackView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private Animation createFadeAnimation(boolean isFadeIn) {
        Animation fadeAnimation = new AlphaAnimation(isFadeIn ? 0 : 1, isFadeIn ? 1 : 0);
        fadeAnimation.setFillAfter(true);
        fadeAnimation.setDuration(FADE_FEEDBACK_DURATION_MS);
        fadeAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        return fadeAnimation;
    }

    private void setQuizFragment() {
        quizBase = QuizFactory.getQuizFragment(QuizSettings.getInstance().getQuizType(), artPieceList);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(quizContainer.getId(), quizBase, quizBase.getClass().getSimpleName()).commit();
    }

    private void updateStatus() {
        final String displayText = MessageFormat.format("{0}/{1}", quizBase.getCurrentArtPieceIndex() + 1, artPieceList.size());
        status.setText(displayText);
    }

    private void updateScore() {
        final String displayText = MessageFormat.format(getString(R.string.score), ResultData.getInstance().getScore());
        scoreTextView.setText(displayText);
        scoreTextView.invalidate();
    }

    private void playSoundFeedback(boolean isCorrect) {
        if (isCorrect) {
            if (!correctSoundPlayer.isPlaying()) {
                correctSoundPlayer.start();
            }
        } else {
            if (!wrongSoundPlayer.isPlaying()) {
                wrongSoundPlayer.start();
            }
        }
    }

    private void setupMediaPlayer() {
        correctSoundPlayer = new MediaPlayer();
        wrongSoundPlayer = new MediaPlayer();

        AssetFileDescriptor correctSoundDescriptor;
        AssetFileDescriptor wrongSoundDescriptor;

        try {
            correctSoundDescriptor = getAssets().openFd("sound/correct.wav");
            wrongSoundDescriptor = getAssets().openFd("sound/wrong.wav");

            correctSoundPlayer.setDataSource(correctSoundDescriptor.getFileDescriptor(), correctSoundDescriptor.getStartOffset(), correctSoundDescriptor.getLength());
            correctSoundPlayer.prepare();

            wrongSoundPlayer.setDataSource(wrongSoundDescriptor.getFileDescriptor(), wrongSoundDescriptor.getStartOffset(), wrongSoundDescriptor.getLength());
            wrongSoundPlayer.prepare();

            if (!QuizSettings.getInstance().isSoundOn()) {
                correctSoundPlayer.setVolume(0,0);
                wrongSoundPlayer.setVolume(0, 0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUserAction(final UserAction action) {
        if (statusBar.getVisibility() == View.GONE) {
            statusBar.setVisibility(View.VISIBLE);
        }

        switch (action) {
            case EVALUATE:
                statusBar.startAnimation(slideAnimation);
                break;
            case NEXT:
                statusBar.startAnimation(slideOutAnimation);
                updateStatus();
                break;
            case FINISH:
                ResultDialogHandler.createDialog(this, new ResultClickHandler() {
                    @Override
                    public void onClickDone() {
                        finish();
                    }

                    @Override
                    public void onClickRetry() {
                        quizBase.handleRetry();
                        statusBar.startAnimation(slideOutAnimation);
                        ResultData.getInstance().clearScore();
                        updateStatus();
                        updateScore();
                    }
                }).show();
                break;
            case EXIT:
                finish();
                break;
        }
    }
}