package hu.kosztandi.gergo.artquiz.view.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.chrisbanes.photoview.PhotoView;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.FileUtilImpl;

public class ImageFragment extends Fragment {
    private static final String IMAGE_NAME = "imageName";
    private PhotoView artImage;
    private Activity context;
    private FileUtil fileUtil;

    public ImageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        return inflater.inflate(R.layout.fragment_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        artImage = view.findViewById(R.id.artPieceImage);

        if (getArguments() != null) {
            fileUtil = new FileUtilImpl(context, () -> {
                updateImage(getArguments().getString(IMAGE_NAME));
            });
        }
    }

    static ImageFragment newInstance(final String initImageUri) {
        ImageFragment fragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_NAME, initImageUri);
        fragment.setArguments(args);
        return fragment;
    }

    final void updateImage(final String imageName) {
        AppUtilities.loadImage(context, fileUtil, artImage, imageName, false);
    }
}
