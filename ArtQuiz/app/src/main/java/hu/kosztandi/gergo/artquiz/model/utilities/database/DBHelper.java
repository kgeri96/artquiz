package hu.kosztandi.gergo.artquiz.model.utilities.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.ArtQuiz;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;

import static hu.kosztandi.gergo.artquiz.model.AppConstants.DATABASE_VERSION;

public class DBHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "ArtDatabase.db";
    private SQLiteDatabase db;
    private String sqlTable;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
        sqlTable = "ArtWork";
    }

    public Cursor getArtPieces() {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect = {"id", "name", "era", "imageURL"};
        qb.setTables(sqlTable);

        Cursor c = qb.query(db, sqlSelect, null, null,
                null, null, null);

        c.moveToFirst();

        return c;
    }

    public Cursor getPossibleEras() {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"era"};
        qb.setTables(sqlTable);
        qb.setDistinct(true);

        return qb.query(db, sqlSelect, null, null,
                null, null, null);
    }

    public Cursor getArtPiecesBySettings() {
        String[] eras = getEraArray();
        String query;
        if(eras == null) {
            query = "SELECT id, name, era, imageURL FROM " + sqlTable;
        } else {
            query = "SELECT id, name, era, imageURL FROM " + sqlTable
                    + " WHERE era IN (" + makePlaceholders(eras.length) + ")";
        }
        return db.rawQuery(query, eras);
    }

    public Cursor getArtPiecesByEra(String[] eras) {
        String query;
        if(eras == null) {
            query = "SELECT id, name, era, imageURL FROM " + sqlTable;
        } else {
            query = "SELECT id, name, era, imageURL FROM " + sqlTable
                    + " WHERE era IN (" + makePlaceholders(eras.length) + ")";
        }
        return db.rawQuery(query, eras);
    }

    /*public Cursor getRandomArtPieces(int numberOfArtPieces, String[] eras) {
        String query = "SELECT DISTINCT id, name, era, imageURL FROM " + sqlTable
                + " WHERE era IN (" + makePlaceholders(eras.length) + ") ORDER BY random() " +
                "LIMIT " + numberOfArtPieces;

        return db.rawQuery(query, eras);
    }*/

    public int getArtPieceCount() {

        String[] eras = getEraArray();
        if(eras == null) {
            String query = "SELECT COUNT(*) FROM " + sqlTable;
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            int count = cursor.getInt(0);
            cursor.close();
            return count;
        } else {
            String query = "SELECT COUNT(*) FROM " + sqlTable
                    + " WHERE era IN (" + makePlaceholders(eras.length) + ")";
            Cursor cursor = db.rawQuery(query, eras);
            cursor.moveToFirst();
            int count = cursor.getInt(0);
            cursor.close();
            return count;
        }
    }

    public int getArtPieceCount(ArrayList<String> selectedEras) {
        String[] eras = getEraArray(selectedEras);
        if(eras == null) {
            String query = "SELECT COUNT(*) FROM " + sqlTable;
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            int count = cursor.getInt(0);
            cursor.close();
            return count;
        } else {
            String query = "SELECT COUNT(*) FROM " + sqlTable
                    + " WHERE era IN (" + makePlaceholders(eras.length) + ")";
            Cursor cursor = db.rawQuery(query, eras);
            cursor.moveToFirst();
            int count = cursor.getInt(0);
            cursor.close();
            return count;
        }
    }

    public boolean insertArtPiece(ArtPiece artPiece) {
        String query = "INSERT INTO " + sqlTable + "(name, era, imageURL)" + " VALUES(?,?,?)";
        String[] artPieceAttrs = {artPiece.getName(), artPiece.getEra(), artPiece.getImageUrl()};
        try {
            db.execSQL(query, artPieceAttrs);
            return true;
        } catch (Exception e) {
            Log.i("Exception", e.getMessage());
            return false;
        }
    }

    public boolean deleteArtPiece(int id) {
        String query = "DELETE FROM " + sqlTable + " WHERE id = ?";
        String[] args = {String.valueOf(id)};
        try {
            db.execSQL(query, args);
            return true;
        } catch (Exception e) {
            Log.i("Exception", e.getMessage());
            return false;
        }
    }

    private String[] getEraArray() {
        String[] eraArray;
        ArrayList<String> selectedEras = QuizSettings.getInstance().getSelectedEras();
        if(selectedEras.contains(ArtQuiz.getContext().getString(R.string.selection_all))) {
            return null;
        } else {
            eraArray = new String[QuizSettings.getInstance().getSelectedEras().size()];
            for(int i = 0; i < selectedEras.size(); i++) {
                eraArray[i] = selectedEras.get(i);
            }
            return eraArray;
        }
    }

    private String[] getEraArray(ArrayList<String> selectedEras) {
        String[] eraArray;
        if(selectedEras.contains(ArtQuiz.getContext().getString(R.string.selection_all))) {
            return null;
        } else {
            eraArray = new String[selectedEras.size()];
            for(int i = 0; i < selectedEras.size(); i++) {
                eraArray[i] = selectedEras.get(i);
            }
            return eraArray;
        }
    }

    private String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }
}