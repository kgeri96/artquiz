package hu.kosztandi.gergo.artquiz.view.fragments;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

public abstract class ImageQuiz extends QuizBase {
    ImageFragment imageFragment;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setImageFragment();
    }

    private void setImageFragment() {
        imageFragment = ImageFragment.newInstance(artPieces.get(currentArtPieceIndex).getImageUrl());
        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(getImageFragmentContainer().getId(), imageFragment, ImageFragment.class.getSimpleName()).commit();
    }
}
