package hu.kosztandi.gergo.artquiz.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hu.kosztandi.gergo.artquiz.ArtQuiz;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;

public class ResultData {
    private static final ResultData ourInstance = new ResultData();
    private HashMap<String, Integer> hitsPerEra;
    private HashMap<String, Integer> totalPerEra;

    public double maxScore = 0;

    public static ResultData getInstance() {
        return ourInstance;
    }

    private ResultData() {
    }

    public void clearScore() {
        hitsPerEra = new HashMap<>();
        totalPerEra = new HashMap<>();

        ArrayList<String> eras = QuizSettings.getInstance().getSelectedEras().size() == 1
                && QuizSettings.getInstance().getSelectedEras().get(0).equals(ArtQuiz.getContext().getString(R.string.selection_all))
                ? ArtPiecesService.getInstance().getAllEras() : QuizSettings.getInstance().getSelectedEras();

        for (final String era : eras) {
            hitsPerEra.put(era, 0);
            totalPerEra.put(era, 0);
        }
    }

    public HashMap<String, Integer> getTotalPerEra() {
        return totalPerEra;
    }

    public HashMap<String, Integer> getHitsPerEra() {
        return hitsPerEra;
    }

    public int getScore() {
        int score = 0;
        for (Map.Entry<String, Integer> entry : hitsPerEra.entrySet()) {
            score += entry.getValue();
        }

        return score;
    }

    public void addHit(final String era, final int score, final int maxScorePerQuestion) {
        final Integer hits = hitsPerEra.get(era);
        final Integer value = totalPerEra.get(era);
        if (hits != null && value != null) {
            hitsPerEra.put(era, hits + score);
            totalPerEra.put(era, value + maxScorePerQuestion);
        } else {
            Log.e(this.getClass().getName(), "Invalid era!");
        }
    }

    public void addMiss(final String era, final int maxScorePerQuestion) {
        final Integer value = totalPerEra.get(era);
        if (value != null) {
            totalPerEra.put(era, value + maxScorePerQuestion);
        } else {
            Log.e(this.getClass().getName(), "Invalid era!");
        }
    }
}
