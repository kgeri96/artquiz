package hu.kosztandi.gergo.artquiz.utils.quiz;

import android.util.Log;
import androidx.annotation.NonNull;
import java.text.MessageFormat;
import hu.kosztandi.gergo.artquiz.ArtQuiz;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.utils.JaroWinklerDistance;

import static hu.kosztandi.gergo.artquiz.model.AppConstants.StringInputAccuracyThreshold;
import static hu.kosztandi.gergo.artquiz.model.AppConstants.answerThreshold;

public class EvaluationResultFactory {
    public static EvaluationResult create(final QuizType quizType, final String userAnswer, final String correctAnswer) {
        return quizType == QuizType.NAME && QuizSettings.getInstance().isNotFullMatchOnly()
                ? getSpecialStringInputEvaluationResult(userAnswer, correctAnswer, quizType) : getSimpleEvaluationResult(userAnswer, correctAnswer, quizType);
    }

    @NonNull
    private static EvaluationResult getSimpleEvaluationResult(final String userAnswer, final String correctAnswer, final QuizType quizType) {
        boolean isCorrect = userAnswer.replace(",","").equals(correctAnswer.replace(",", ""));

        return new EvaluationResult(isCorrect ? EvaluationResponse.CORRECT : EvaluationResponse.INCORRECT, isCorrect
                ? ArtQuiz.getContext().getString(R.string.answer_correct) : MessageFormat.format(ArtQuiz.getContext().getString(R.string.answer_incorrect), correctAnswer), quizType);
    }

    @NonNull
    private static EvaluationResult getSpecialStringInputEvaluationResult(final String userAnswer, final String correctAnswer, final QuizType quizType) {
        String message;
        EvaluationResponse response;
        switch (getStringInputAccuracy(correctAnswer, userAnswer)) {
            case FULL:
                response = EvaluationResponse.CORRECT;
                message = ArtQuiz.getContext().getString(R.string.answer_correct);
                break;
            case CASE_ERROR:
                response = EvaluationResponse.PARTLY_CORRECT;
                message = MessageFormat.format(ArtQuiz.getContext().getString(R.string.quiz_capital_alert), correctAnswer);
                break;
            case CLOSE_MATCH:
                response = EvaluationResponse.PARTLY_CORRECT;
                message = MessageFormat.format(ArtQuiz.getContext().getString(R.string.quiz_similar_alert), correctAnswer);
                break;
            default:
                response = EvaluationResponse.INCORRECT;
                message = MessageFormat.format(ArtQuiz.getContext().getString(R.string.answer_incorrect), correctAnswer);
                break;
        }

        return new EvaluationResult(response, message, quizType);
    }

    private static AnswerAccuracy getStringInputAccuracy(final String artPieceName, final String userAnswer) {
        final JaroWinklerDistance jaroWinklerDistance = new JaroWinklerDistance();
        double similar = jaroWinklerDistance.apply(artPieceName, userAnswer);
        Log.i("Similarity", String.valueOf(similar));

        if (artPieceName.equalsIgnoreCase(userAnswer) && !artPieceName.equals(userAnswer)) {
            return AnswerAccuracy.CASE_ERROR;
        }

        if (Math.abs(artPieceName.length() - userAnswer.length()) > answerThreshold) {
            return AnswerAccuracy.NONE;
        }

        if (artPieceName.equals(userAnswer)) {
            return AnswerAccuracy.FULL;
        }

        if (similar > StringInputAccuracyThreshold) {
            return AnswerAccuracy.CLOSE_MATCH;
        }

        return AnswerAccuracy.NONE;
    }

    private enum AnswerAccuracy {
        FULL, CASE_ERROR, CLOSE_MATCH, NONE
    }
}
