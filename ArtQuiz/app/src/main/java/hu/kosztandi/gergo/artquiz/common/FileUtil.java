package hu.kosztandi.gergo.artquiz.common;

import java.io.File;

public interface FileUtil {
    /**
     *
     * @return if expansion file is found
     */
    boolean checkExpansionFile();

    /**
     * Check expansionFile
     * @param fileName name of file
     * @return File or null
     */

    File getImageByName(String fileName);
}
