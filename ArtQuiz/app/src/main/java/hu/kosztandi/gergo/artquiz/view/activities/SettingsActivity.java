package hu.kosztandi.gergo.artquiz.view.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;
import hu.kosztandi.gergo.artquiz.utils.quiz.QuizType;
import hu.kosztandi.gergo.artquiz.model.settings.OnCheckClick;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;
import hu.kosztandi.gergo.artquiz.view.adapters.EraAdapter;

public class SettingsActivity extends AppCompatActivity {
    private TextView learnModeText;
    private Switch learnMode;
    private Switch isSoundOn;
    private EraAdapter listAdapter;
    private Spinner quizTypeSpinner;
    private EditText editText;
    private CheckBox fullMatchCheckBox;
    private Button apply;

    private ViewGroup limitContainer;
    private ViewGroup quizTypeContainer;
    private ViewGroup fullMatchContainer;

    private long totalArtPieceCount;
    private long limit;

    private static final String ALL_ERAS_SELECTED = "Összes";
    private static final int NAME_TYPE_INDEX = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        setupReferences();
        setupApplyButton();
        setupLearnModeListener();
        loadData();

        AppUtilities.loadImageToContainer(findViewById(R.id.settingsRoot), R.drawable.neutral_bg);
    }

    private void setupReferences() {
        apply = findViewById(R.id.apply);
        editText = findViewById(R.id.numberOfQuestionsEdit);
        quizTypeSpinner = findViewById(R.id.quizTypeSpinner);
        fullMatchCheckBox = findViewById(R.id.checkBox);
        limitContainer = findViewById(R.id.limitContainer);
        quizTypeContainer = findViewById(R.id.quizTypeContainer);
        learnMode = findViewById(R.id.learnModeSwitch);
        isSoundOn = findViewById(R.id.soundSwitch);
        eraList = findViewById(R.id.eraGrid);
        learnModeText = findViewById(R.id.learnModeText);
        fullMatchContainer = findViewById(R.id.fullMatchContainer);
    }

    private void setupApplyButton() {
        apply.setOnClickListener(view -> applySettings());
    }

    private void setupLearnModeListener() {
        learnMode.setOnCheckedChangeListener((compoundButton, b) -> switchLearnUIVisibility(b));
    }

    private void switchLearnUIVisibility(boolean isChecked) {
        if(isChecked) {
            limitContainer.setVisibility(View.GONE);
            quizTypeContainer.setVisibility(View.GONE);
            fullMatchCheckBox.setVisibility(View.GONE);
            fullMatchContainer.setVisibility(View.GONE);
            learnModeText.setVisibility(View.VISIBLE);
        } else {
            limitContainer.setVisibility(View.VISIBLE);
            quizTypeContainer.setVisibility(View.VISIBLE);
            fullMatchCheckBox.setVisibility(View.VISIBLE);
            fullMatchContainer.setVisibility(getSelectedQuizType() != QuizType.NAME ? View.GONE : View.VISIBLE);
            learnModeText.setVisibility(View.GONE);
        }
    }

    private void applySettings() {
        QuizSettings.getInstance().setLearnMode(learnMode.isChecked());
        QuizSettings.getInstance().setSoundOn(isSoundOn.isChecked());
        QuizType selectedQuizType;
        selectedQuizType = getSelectedQuizType();

        QuizSettings.getInstance().setQuizType(selectedQuizType);
        QuizSettings.getInstance().setNotFullMatchOnly(fullMatchCheckBox.isChecked());

        long questionLimit = getEditTextValue();
        QuizSettings.getInstance().setQuestionLimit(questionLimit);

        ArrayList<String> selectedEras = getSelectedEras();

        if(selectedEras.size() == 0) {
            Toast.makeText(this, R.string.era_selection_warning,Toast.LENGTH_SHORT).show();
        } else {
            QuizSettings.getInstance().setSelectedEras(selectedEras);
            savePrefs();
            finish();
        }
    }

    @NonNull
    private QuizType getSelectedQuizType() {
        QuizType selectedQuizType;
        switch (quizTypeSpinner.getSelectedItemPosition()) {
            case 0:
                selectedQuizType = QuizType.ERA;
                break;
            case 1:
                selectedQuizType = QuizType.NAME;
                break;
            case 2:
                selectedQuizType = QuizType.MULTIPLE_SELECTION;
                break;
            default:
                selectedQuizType = QuizType.NAME;
                break;
        }
        return selectedQuizType;
    }

    private void savePrefs() {
        QuizSettings.getInstance().savePrefs();
    }

    private long getEditTextValue() {
        try {
            return Long.parseLong(editText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private ArrayList<String> getSelectedEras() {
        boolean[] checkStatus = listAdapter.getCheckStatus();
        ArrayList<String> selectedEras = new ArrayList<>();
        ArrayList<String> allEras = ArtPiecesService.getInstance().getAllEras();
        for (int i = 0; i < checkStatus.length; i++) {
            if (i == checkStatus.length-1 && checkStatus[i]) {
                selectedEras.clear();
                selectedEras.add(getString(R.string.selection_all));
            } else {
                if (checkStatus[i]) {
                    selectedEras.add(allEras.get(i));
                }
            }
        }
        return selectedEras;
    }

    private void loadData() {
        setupEraList();
        setupSpinner();
        setupNumberInput();
        setSwitches();
        setCheck();
    }

    private void setCheck() {
        fullMatchCheckBox.setChecked(QuizSettings.getInstance().isNotFullMatchOnly());
    }

    private void setSwitches() {
        learnMode.setChecked(QuizSettings.getInstance().isLearnMode());
        isSoundOn.setChecked(QuizSettings.getInstance().isSoundOn());
    }

    private void setupSpinner() {
        fullMatchContainer.setVisibility(QuizSettings.getInstance().getQuizType() != QuizType.NAME ? View.GONE : View.VISIBLE);
        QuizType quizType = QuizSettings.getInstance().getQuizType();

        switch (quizType) {
            case ERA:
                quizTypeSpinner.setSelection(0);
                break;
            case NAME:
                quizTypeSpinner.setSelection(1);
                break;
            case MULTIPLE_SELECTION:
                quizTypeSpinner.setSelection(2);
                break;
            default:
        }

        quizTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != NAME_TYPE_INDEX) {
                    fullMatchContainer.setVisibility(View.GONE);
                } else {
                    fullMatchContainer.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    long questionLimit;
    private void setupNumberInput() {
        questionLimit = QuizSettings.getInstance().getQuestionLimit();
        editText.setText(String.valueOf(questionLimit));
        editText.addTextChangedListener(new TextWatcher() {
            private long before;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().isEmpty()) {
                    try {
                        before = Long.parseLong(charSequence.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()) {
                    if(totalArtPieceCount == 0) {
                        updateLimit();
                    }
                    try {
                        if (totalArtPieceCount != 0) {
                            long parsedValue = Long.parseLong(editable.toString());
                            if(parsedValue > totalArtPieceCount) {
                                editText.setText(String.valueOf(totalArtPieceCount));
                            } else if(parsedValue <= 0) {
                                editText.setText(String.valueOf(1));
                            }
                        } else {
                            editText.setText(String.valueOf(1));
                        }

                    } catch (Exception e) {
                        editText.setText(String.valueOf(before));
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    ListView eraList;
    private void setupEraList() {
        final ArrayList<String> eras = ArtPiecesService.getInstance().getAllEras();
        if(!eras.contains(ALL_ERAS_SELECTED)) {
            eras.add(ALL_ERAS_SELECTED);
        }

        final CheckListener listener = new CheckListener();
        listAdapter = new EraAdapter(this, eras, listener);
        eraList.setAdapter(listAdapter);
    }

    private void updateLimitText() {
        long value;
        try {
            if(!editText.getText().toString().isEmpty()) {
                value = Long.parseLong(editText.getText().toString());
            } else {
                editText.setText(String.valueOf(1));
                value = 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            editText.setText(String.valueOf(1));
            value = 1;
        }
        if(value > limit) {
            editText.setText(String.valueOf(limit));
        }
    }

    public void updateLimit() {
        if(getSelectedEras().size() > 0) {
            totalArtPieceCount = ArtPiecesService.getInstance().getArtPieceCount(getSelectedEras());
        }

        limit = getEditTextValue();
        if(limit > totalArtPieceCount) {
            limit = totalArtPieceCount;
        }
    }

    private class CheckListener implements OnCheckClick {
        @Override
        public void onClick(int position, boolean isChecked) {
            updateLimit();
            updateLimitText();
        }
    }
}
