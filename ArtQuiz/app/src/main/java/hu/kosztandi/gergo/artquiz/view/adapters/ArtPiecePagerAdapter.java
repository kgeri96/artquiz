package hu.kosztandi.gergo.artquiz.view.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;
import hu.kosztandi.gergo.artquiz.common.BottomBarChangeListener;

public class ArtPiecePagerAdapter extends PagerAdapter {
    private ArrayList<ArtPiece> artPieces;
    private PhotoView artPieceImage;
    private Context context;
    private BottomBarChangeListener listener;
    private FileUtil fileUtil;

    public ArtPiecePagerAdapter(Context context, ArrayList<ArtPiece> artPieces, BottomBarChangeListener listener, FileUtil fileUtil) {
        this.context = context;
        this.artPieces = artPieces;
        this.listener = listener;
        this.fileUtil = fileUtil;
    }

    @Override
    public int getCount() {
        return artPieces.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final ArtPiece artPiece = artPieces.get(position);
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.art_piece_pager_item, container, false);
        setUIComponents(layout, artPiece);

        container.addView(layout);
        return layout;
    }

    private void setUIComponents(ViewGroup layout, ArtPiece artPiece) {
        setReferences(layout);
        setContent(artPiece);
    }

    private void setContent(ArtPiece artPiece) {
        AppUtilities.loadImage(context, fileUtil, artPieceImage, artPiece.getImageUrl(), false);
        setTapListener();
    }

    private void setTapListener() {
        artPieceImage.setOnViewTapListener((view, x, y) -> listener.switchVisibility());
    }

    private void setReferences(ViewGroup layout) {
        artPieceImage = layout.findViewById(R.id.artPieceImage);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
