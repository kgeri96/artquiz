package hu.kosztandi.gergo.artquiz.view.activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.qtalk.recyclerviewfastscroller.RecyclerViewFastScroller;

import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.FileUtil;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.model.utilities.AppUtilities;
import hu.kosztandi.gergo.artquiz.common.BackgroundTask;
import hu.kosztandi.gergo.artquiz.common.OnClickRecyclerItem;
import hu.kosztandi.gergo.artquiz.model.utilities.IObbMountedListener;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.FileUtilImpl;
import hu.kosztandi.gergo.artquiz.model.utilities.implementation.QuizPreparatorTask;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;
import hu.kosztandi.gergo.artquiz.view.adapters.ImageHandlerAdapter;

public class ImageHandlerActivity extends AppCompatActivity {
    private static final int READ_REQUEST_CODE = 42;
    private static final int READ_STORAGE_REQUEST = 43;

    private ImageHandlerAdapter adapter;
    private ArrayList<ArtPiece> artPieces;
    private Uri addUri;
    private FileUtil fileUtil;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_handler);

        AppUtilities.loadImageToContainer(findViewById(R.id.imageHandlerRoot), R.drawable.neutral_bg);

        new QuizPreparatorTask(new BackgroundTask() {
            @Override
            public void runTask() {
                artPieces = ArtPiecesService.getInstance().getAllArtPieces();
            }

            @Override
            public void onFinish() {
                fileUtil = new FileUtilImpl(ImageHandlerActivity.this, () -> {
                    setButtons();
                    setListAdapter();
                    setRecyclerView();
                });
            }
        }, this).execute();
    }

    private void setButtons() {
        final ConstraintLayout constraintLayout = findViewById(R.id.addImageContainer);

        constraintLayout.setOnClickListener(view ->
                AppUtilities.askForPermissionAndExecuteCommand(this, READ_STORAGE_REQUEST,Manifest.permission.READ_EXTERNAL_STORAGE,
                R.string.read_permission_request_text, this::selectFile));
    }

    private void setRecyclerView() {
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        final RecyclerViewFastScroller scroller = findViewById(R.id.fastScroller);
        final Drawable handler = scroller.getHandleDrawable();

        if (handler != null) {
            handler.setAlpha(0);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final @NonNull RecyclerView recyclerView, final int newState) {
                animateHandler(handler, newState != RecyclerView.SCROLL_STATE_IDLE);
            }
        });
    }

    private void animateHandler(final Drawable handler, boolean showHandler) {
        if (handler != null) {
            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(handler, PropertyValuesHolder.ofInt("alpha", showHandler ? 255 : 0));
            animator.setTarget(handler);
            animator.setDuration(300);
            animator.start();
        }
    }

    private void setListAdapter() {
        adapter = new ImageHandlerAdapter(artPieces, new DeleteClickHandler(), fileUtil);
    }

    private void selectFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public final void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri;
            if (resultData != null) {
                uri = resultData.getData();
                if (uri != null) {
                    addUri = uri;
                    createInsertDialog();
                } else {
                    Toast.makeText(this, R.string.invalid_file_selection, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void createInsertDialog() {
        final Dialog dialog = getDialog();
        setOkButton(dialog);
        dialog.show();
    }

    private Dialog getDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.insert_dialog);
        return dialog;
    }

    private void setOkButton(final Dialog dialog) {
        Button okButton = dialog.findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            private EditText nameEditText = dialog.findViewById(R.id.nameEditText);
            private EditText eraEditText = dialog.findViewById(R.id.eraEditText);

            @Override
            public void onClick(View view) {
                if (checkEditContent()) {
                    handleInsertion();
                } else {
                    Toast.makeText(ImageHandlerActivity.this, R.string.image_handler_name_era_mandatory, Toast.LENGTH_LONG).show();
                }
            }

            private void handleInsertion() {
                ArtPiece artPiece = new ArtPiece(nameEditText.getText().toString(), eraEditText.getText().toString(), addUri.toString());
                if (ArtPiecesService.getInstance().insertArtpiece(artPiece)) {
                    artPieces.add(artPiece);
                    notifyAdapter();
                    dialog.dismiss();
                } else {
                    Toast.makeText(ImageHandlerActivity.this, R.string.image_handler_insert_failure, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }

            private boolean checkEditContent() {
                return nameEditText.getText().toString().length() > 0 && eraEditText.getText().toString().length() > 0;
            }
        });
    }


    private void notifyAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public final void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == READ_STORAGE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectFile();
            } else {
                Toast.makeText(this, R.string.read_permission_request_text, Toast.LENGTH_LONG).show();
            }
        }
    }

    private class DeleteClickHandler implements OnClickRecyclerItem {
        @Override
        public void onClick(int pos) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ImageHandlerActivity.this);

            setupDialog(pos, alertDialogBuilder);
            alertDialogBuilder.create().show();
        }

        private void setupDialog(final int position, AlertDialog.Builder alertDialogBuilder) {
            alertDialogBuilder
                    .setTitle(R.string.delete_artpiece_dialog_title)
                    .setMessage(R.string.delete_dialog_title)
                    .setPositiveButton(R.string.dialog_positive, (dialogInterface, i) -> {
                        if (ArtPiecesService.getInstance().deleteArtpiece(artPieces.get(position))) {
                            QuizSettings.getInstance().updateSelectedErasAndLimit(artPieces.get(position).getEra());
                            artPieces.remove(position);
                            notifyAdapter();
                        } else {
                            Toast.makeText(ImageHandlerActivity.this, "Ismeretlen hiba történt", Toast.LENGTH_LONG).show();
                        }
                    })
                    .setNegativeButton(R.string.dialog_negative, (dialogInterface, i) -> dialogInterface.dismiss());
        }
    }
}
