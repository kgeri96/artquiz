package hu.kosztandi.gergo.artquiz.common;

public interface BackgroundTask {
    void runTask();
    void onFinish();
}
