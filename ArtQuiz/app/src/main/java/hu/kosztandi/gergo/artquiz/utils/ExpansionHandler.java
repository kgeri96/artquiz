package hu.kosztandi.gergo.artquiz.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Messenger;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.google.android.vending.expansion.downloader.Constants;
import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;
import com.google.android.vending.expansion.zipfile.ZipResourceFile;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.zip.CRC32;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.ExpansionHandlerListener;
import hu.kosztandi.gergo.artquiz.service.ExpansionDownloaderService;

public class ExpansionHandler implements IDownloaderClient {
    private static String LOG_TAG = ExpansionHandler.class.getName();
    static private final float SMOOTHING_FACTOR = 0.005f;
    public static boolean cancelValidation;

    private Activity context;
    private ExpansionHandlerListener listener;
    private IStub downloaderClientStub;
    private IDownloaderService remoteService;
    private String currentStatus;

    private int currentState;
    private boolean isPaused;

    public ExpansionHandler(final Activity context, final ExpansionHandlerListener listener) {
        this.context = context;
        this.listener = listener;

        currentStatus = context.getString(R.string.splash_loading);
        isPaused = false;

        startValidation();
    }

    private void startValidation() {
        if (!xAPKFilesDelivered() && Helpers.canWriteOBBFile(context)) {
            launchDownloader();
        } else if (!xAPKFilesReadable()) {
            Toast.makeText(context, R.string.apk_read_error, Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG, "Could not read apk");
        } else {
            validateXAPKZipFiles(context, listener);
        }
    }

    private static void validateXAPKZipFiles(final Context context, ExpansionHandlerListener listener) {
        final AsyncTask<Object, DownloadProgressInfo, Boolean> validationTask = new AsyncTask<Object, DownloadProgressInfo, Boolean>() {
                    @Override
                    protected void onPreExecute() {
                        listener.onValidationStarted();
                        super.onPreExecute();
                    }

                    @Override
                    protected Boolean doInBackground(Object... params) {
                        for (XAPKFile xf : xAPKS) {
                            String fileName = Helpers.getExpansionAPKFileName(context, xf.mIsMain, xf.mFileVersion);
                            if (!Helpers.doesFileExist(context, fileName, xf.mFileSize, false)) {
                                return false;
                            }
                            fileName = Helpers.generateSaveFileName(context, fileName);
                            ZipResourceFile zrf;
                            byte[] buf = new byte[1024 * 256];
                            try {
                                zrf = new ZipResourceFile(fileName);
                                ZipResourceFile.ZipEntryRO[] entries = zrf.getAllEntries();
                                /*
                                 * First calculate the total compressed length
                                 */
                                long totalCompressedLength = 0;
                                for (ZipResourceFile.ZipEntryRO entry : entries) {
                                    totalCompressedLength += entry.mCompressedLength;
                                }
                                float averageVerifySpeed = 0;
                                long totalBytesRemaining = totalCompressedLength;
                                long timeRemaining;
                                /*
                                  Then calculate a CRC for every file in the
                                  Zip file, comparing it to what is stored in
                                  the Zip directory. Note that for compressed
                                  Zip files we must extract the contents to do
                                  this comparison.
                                 */
                                for (ZipResourceFile.ZipEntryRO entry : entries) {
                                    if (-1 != entry.mCRC32) {
                                        long length = entry.mUncompressedLength;
                                        CRC32 crc = new CRC32();
                                        DataInputStream dis = null;
                                        try {
                                            dis = new DataInputStream(
                                                    zrf.getInputStream(entry.mFileName));

                                            long startTime = SystemClock.uptimeMillis();
                                            while (length > 0) {
                                                int seek = (int) (length > buf.length ? buf.length
                                                        : length);
                                                dis.readFully(buf, 0, seek);
                                                crc.update(buf, 0, seek);
                                                length -= seek;
                                                long currentTime = SystemClock.uptimeMillis();
                                                long timePassed = currentTime - startTime;
                                                if (timePassed > 0) {
                                                    float currentSpeedSample = (float) seek
                                                            / (float) timePassed;
                                                    if (0 != averageVerifySpeed) {
                                                        averageVerifySpeed = SMOOTHING_FACTOR
                                                                * currentSpeedSample
                                                                + (1 - SMOOTHING_FACTOR)
                                                                * averageVerifySpeed;
                                                    } else {
                                                        averageVerifySpeed = currentSpeedSample;
                                                    }
                                                    totalBytesRemaining -= seek;
                                                    timeRemaining = (long) (totalBytesRemaining / averageVerifySpeed);
                                                    this.publishProgress(
                                                            new DownloadProgressInfo(
                                                                    totalCompressedLength,
                                                                    totalCompressedLength
                                                                            - totalBytesRemaining,
                                                                    timeRemaining,
                                                                    averageVerifySpeed)
                                                    );
                                                }
                                                startTime = currentTime;

                                                if (cancelValidation) {
                                                    return true;
                                                }
                                            }
                                            if (crc.getValue() != entry.mCRC32) {
                                                Log.e(Constants.TAG, "CRC does not match for entry: " + entry.mFileName);
                                                Log.e(Constants.TAG,"In file: " + entry.getZipFileName());
                                                return false;
                                            }
                                        } finally {
                                            if (null != dis) {
                                                dis.close();
                                            }
                                        }
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        return true;
                    }

                    @Override
                    protected void onProgressUpdate(DownloadProgressInfo... values) {
                        listener.onStateChanged(values[0].toString());
                        super.onProgressUpdate(values);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        super.onPostExecute(result);
                        listener.onTasksCompleted();
                    }

                };
        validationTask.execute(new Object());
    }

    private void launchDownloader() {
        try {
            Intent launchIntent = context.getIntent();

            if (launchIntent.getCategories() != null) {
                for (String category : launchIntent.getCategories()) {
                    launchIntent.addCategory(category);
                }
            }

            if (DownloaderClientMarshaller.startDownloadServiceIfRequired(context, PendingIntent.getActivity(context, 0, launchIntent, PendingIntent.FLAG_UPDATE_CURRENT),
                    ExpansionDownloaderService.class) != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
                listener.onStartDownload();
                downloaderClientStub = DownloaderClientMarshaller.CreateStub
                        (this, ExpansionDownloaderService.class);
                downloaderClientStub.connect(context);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Cannot find own package! WTF");
            e.printStackTrace();
        }
    }

    private boolean xAPKFilesReadable() {
        for (final XAPKFile xf : xAPKS) {
            final String fileName = Helpers.getExpansionAPKFileName(context, xf.mIsMain, xf.mFileVersion);
            if ( Helpers.getFileStatus(context, fileName) == Helpers.FS_CANNOT_READ ) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onServiceConnected(final Messenger m) {
        remoteService = DownloaderServiceMarshaller.CreateProxy(m);
        remoteService.onClientUpdated(downloaderClientStub.getMessenger());
        listener.onServiceConnected();
    }

    @Override
    public void onDownloadStateChanged(int newState) {
        setState(newState);

        switch (newState) {
            case IDownloaderClient.STATE_IDLE:
                break;
            case IDownloaderClient.STATE_CONNECTING:
            case IDownloaderClient.STATE_FETCHING_URL:
                break;
            case IDownloaderClient.STATE_DOWNLOADING:
                break;

            case IDownloaderClient.STATE_FAILED_CANCELED:
            case IDownloaderClient.STATE_FAILED:
            case IDownloaderClient.STATE_FAILED_FETCHING_URL:
            case IDownloaderClient.STATE_FAILED_UNLICENSED:
                break;
            case IDownloaderClient.STATE_PAUSED_NEED_CELLULAR_PERMISSION:
                listener.onServiceNeedsCellular();
            case IDownloaderClient.STATE_PAUSED_WIFI_DISABLED_NEED_CELLULAR_PERMISSION:
                break;
            case IDownloaderClient.STATE_PAUSED_WIFI_DISABLED:
            case IDownloaderClient.STATE_PAUSED_NEED_WIFI:
                break;

            case IDownloaderClient.STATE_PAUSED_BY_REQUEST:
                break;
            case IDownloaderClient.STATE_PAUSED_ROAMING:
            case IDownloaderClient.STATE_PAUSED_SDCARD_UNAVAILABLE:
                break;
            case IDownloaderClient.STATE_COMPLETED:
                validateXAPKZipFiles(context, listener);
                return;
            default:
        }
    }

    @Override
    public void onDownloadProgress(DownloadProgressInfo progress) {
        Log.i(LOG_TAG, ""+progress);
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;

        if (isPaused) {
            remoteService.requestPauseDownload();
        } else {
            remoteService.requestContinueDownload();
        }
    }

    public void continueCellular() {
        remoteService.setDownloadFlags(IDownloaderService.FLAGS_DOWNLOAD_OVER_CELLULAR);
        remoteService.requestContinueDownload();
        isPaused = false;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    private void setState(int newState) {
        if (currentState != newState) {
            currentState = newState;
            currentStatus = context.getString(Helpers.getDownloaderStringResourceIDFromState(newState));
            listener.onStateChanged(currentStatus);
        }
    }

    private static class XAPKFile {
        final boolean mIsMain;
        final int mFileVersion;
        final long mFileSize;

        XAPKFile(boolean isMain, int fileVersion, long fileSize) {
            mIsMain = isMain;
            mFileVersion = fileVersion;
            mFileSize = fileSize;
        }
    }

    private static final XAPKFile[] xAPKS = {
            new XAPKFile(true, 1, 121724928L)
    };

    private boolean xAPKFilesDelivered() {
        for (XAPKFile file : xAPKS) {
            String fileName = Helpers.getExpansionAPKFileName(context, file.mIsMain, file.mFileVersion);
            if (!Helpers.doesFileExist(context, fileName, file.mFileSize, true))
                return false;
        }
        return true;
    }
}
