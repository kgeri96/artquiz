package hu.kosztandi.gergo.artquiz.common;

public interface ExpansionHandlerListener {
    void onStartDownload();
    void onStateChanged(String status);
    void onServiceNeedsCellular();
    void onServiceConnected();
    void onTasksCompleted();
    void onValidationStarted();
}
