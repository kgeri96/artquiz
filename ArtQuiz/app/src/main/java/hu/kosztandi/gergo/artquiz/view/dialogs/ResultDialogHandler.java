package hu.kosztandi.gergo.artquiz.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.ResultClickHandler;
import hu.kosztandi.gergo.artquiz.utils.ResultData;
import hu.kosztandi.gergo.artquiz.view.CircleProgressBar;
import hu.kosztandi.gergo.artquiz.view.adapters.ResultPerEraAdapter;

public class ResultDialogHandler {
    private static CircleProgressBar progressBar;
    public static Dialog createDialog(final Context context, final ResultClickHandler handler) {
        final Dialog dialog = initDialog(context);
        setResult(dialog);
        setDoneButton(handler, dialog);
        setRetryButton(handler, dialog);
        return dialog;
    }

    private static Dialog initDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.result_view);
        progressBar = dialog.findViewById(R.id.circleProgressBar2);

        ListView listView = dialog.findViewById(R.id.detailsListView);
        listView.setAdapter(new ResultPerEraAdapter());

        ImageView dropdownIcon = dialog.findViewById(R.id.dropdownIcon);
        ViewGroup details = dialog.findViewById(R.id.details);
        details.setOnClickListener(view -> {
            if (listView.getVisibility() == View.VISIBLE) {
                listView.setVisibility(View.GONE);
                dropdownIcon.setImageResource(R.drawable.ic_arrow_drop_down);
            } else {
                listView.setVisibility(View.VISIBLE);
                dropdownIcon.setImageResource(R.drawable.ic_arrow_drop_up);
            }
        });

        dialog.setCancelable(false);
        return dialog;
    }

    private static void setRetryButton(final ResultClickHandler handler, final Dialog dialog) {
        Button retryButton = dialog.findViewById(R.id.retryButton);
        retryButton.setOnClickListener(view -> {
                handler.onClickRetry();
                dialog.dismiss();
            });
    }

    private static void setDoneButton(final ResultClickHandler handler, final Dialog dialog) {
        Button doneButton = dialog.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(view -> {
                handler.onClickDone();
                dialog.dismiss();
            });
    }

    private static void setResult(Dialog dialog) {
        TextView resultTextView = dialog.findViewById(R.id.result);
        double hitPercent = ResultData.getInstance().getScore() / ResultData.getInstance().maxScore * 100;
        progressBar.setProgress((float)hitPercent, resultTextView);
    }
}