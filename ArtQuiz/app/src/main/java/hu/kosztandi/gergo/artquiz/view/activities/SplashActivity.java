package hu.kosztandi.gergo.artquiz.view.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;

import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;

import java.util.ArrayList;
import java.util.List;

import hu.kosztandi.gergo.artquiz.BuildConfig;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.ExpansionHandlerListener;
import hu.kosztandi.gergo.artquiz.utils.ExpansionHandler;

public class SplashActivity extends AppCompatActivity {
    private static final int EXPANSION_REQUEST = 10;
    private static final String EXPANSION_FILE_NAME = "main." + BuildConfig.VERSION_CODE + "." + BuildConfig.APPLICATION_ID + ".obb";
    private static final String LOG_TAG = "SPLASH";

    private FrameLayout logoContainer;
    private TextView progressText;
    private TextView progressPercent;
    private ImageButton pauseButton;

    private Intent mainIntent;

    private boolean allDenied;
    private ExpansionHandler expansionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        logoContainer = findViewById(R.id.logoContainer);
        mainIntent = new Intent(this, MainActivity.class);

        pauseButton = findViewById(R.id.pauseButton);
        progressText = findViewById(R.id.progressText);
        progressPercent = findViewById(R.id.progressPercent);
        allDenied = false;

        setRipple();
        checkPermissions();
    }

    private void initDownloadUI() {
        pauseButton.setOnClickListener(v -> {
            if (expansionHandler.isPaused()) {
                pauseButton.setImageDrawable(getDrawable(R.drawable.ic_pause));
                progressText.setText(expansionHandler.getCurrentStatus());
            } else {
                pauseButton.setImageDrawable(getDrawable(R.drawable.ic_play_arrow));
                progressText.setText(R.string.splash_paused);
            }

            expansionHandler.setPaused(!expansionHandler.isPaused());
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean deniedPermission = false;

        if (permissions.length > 0 && grantResults.length > 0) {
            for (int result : grantResults) {
                if (result == PackageManager.PERMISSION_DENIED) {
                    deniedPermission = true;
                    break;
                }
            }
        } else {
            Toast.makeText(this, R.string.permission_interrupted, Toast.LENGTH_SHORT).show();
        }

        if (deniedPermission) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED && ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]) && !allDenied) {
                    showPermissionJustification(permissions[i]);
                    break;
                } else {
                    closeOnDenied();
                }
            }
        } else {
            startExpansionHandler();
        }
    }

    private void startExpansionHandler() {
        expansionHandler = new ExpansionHandler(this, new ExpansionHandlerListener() {

            @Override
            public void onStartDownload() {
                initDownloadUI();
            }

            @Override
            public void onStateChanged(String status) {
                progressText.setText(status);
            }

            @Override
            public void onServiceNeedsCellular() {
                checkCellular();
            }

            @Override
            public void onServiceConnected() {
                checkCellular();
            }

            @Override
            public void onTasksCompleted() {
                startActivity(mainIntent);
            }

            @Override
            public void onValidationStarted() {
                progressText.setText(R.string.validate_image_expansion);
                pauseButton.setImageDrawable(getDrawable(R.drawable.ic_cancel));
                pauseButton.setOnClickListener(v -> ExpansionHandler.cancelValidation = true);
            }
        });
    }

    private void showPermissionJustification(final String permission) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE) || permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            builder.setMessage(R.string.read_write_permission_request_on_load_text)
                    .setTitle(R.string.permission_title)
                    .setPositiveButton(R.string.button_positive, (dialog, which) -> checkPermissions())
                    .setNegativeButton(R.string.text_button_cancel, (dialog, which) -> closeOnDenied())
                    .create().show();
        } else {
            throw new RuntimeException("Handling of permission was not implemented!");
        }
    }

    private void closeOnDenied() {
        Toast.makeText(this, R.string.required_permission_denied, Toast.LENGTH_LONG).show();
        new Handler().postDelayed(this::finish, 2000);
    }

    private void checkPermissions() {
        handlePermissionOperation(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE } );
    }

    private void handlePermissionOperation(final String[] permissionKeys) {
        final List<String> requestPermissions = new ArrayList<>();

        for (final String permission : permissionKeys) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions.add(permission);
            }
        }

        if (requestPermissions.isEmpty()) {
            startExpansionHandler();
        } else {
            ActivityCompat.requestPermissions(this, requestPermissions.toArray(new String[0]), EXPANSION_REQUEST);
        }
    }

    private void setRipple() {
        MultipleRippleLoader multipleRippleLoader = new MultipleRippleLoader(this,
                getResources().getDimensionPixelSize(R.dimen.splash_logo_container) / 5,
                ContextCompat.getColor(this, R.color.mainDark),
                3);

        multipleRippleLoader.setFromAlpha(0.9f);
        multipleRippleLoader.setToAlpha(0f);
        multipleRippleLoader.setAnimationDuration(4200);
        multipleRippleLoader.setInterpolator(new LinearOutSlowInInterpolator());

        logoContainer.addView(multipleRippleLoader);
    }

    private void checkCellular() {
        final ConnectivityManager connectivityManager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());

            if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                expansionHandler.setPaused(true);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.network_alert)
                        .setMessage(R.string.cellular_alert)
                        .setPositiveButton(R.string.continue_button, (dialog, which) -> expansionHandler.continueCellular())
                        .setNegativeButton(R.string.text_button_wifi_settings, (dialog, which) -> startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)))
                        .create().show();
            }
        }
    }
}
