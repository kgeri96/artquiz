package hu.kosztandi.gergo.artquiz.utils.quiz;

import static hu.kosztandi.gergo.artquiz.model.AppConstants.SCORE_NAME;
import static hu.kosztandi.gergo.artquiz.model.AppConstants.SCORE_NAME_INACCURATE_ANSWER;
import static hu.kosztandi.gergo.artquiz.model.AppConstants.SCORE_SIMPLE;

public class EvaluationResult {

    public EvaluationResponse response;
    public String message;
    public int score;

    public EvaluationResult(final EvaluationResponse response, final String message, final QuizType quizType) {
        this.response = response;
        this.message = message;
        setScore(response, quizType);
    }

    private void setScore(EvaluationResponse response, QuizType quizType) {
        if (quizType == QuizType.NAME) {
            if (response == EvaluationResponse.CORRECT) {
                score = SCORE_NAME;
            } else if (response == EvaluationResponse.PARTLY_CORRECT) {
                score = SCORE_NAME_INACCURATE_ANSWER;
            }

            score = 0;
        } else {
            score = response == EvaluationResponse.CORRECT ? SCORE_SIMPLE : 0;
        }
    }
}

