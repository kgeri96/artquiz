package hu.kosztandi.gergo.artquiz.view;

import android.content.Context;
import android.util.AttributeSet;
import hu.kosztandi.gergo.artquiz.R;

public class MultipleSelectionButton extends androidx.appcompat.widget.AppCompatButton {
    private static final int[] STATE_CORRECT = { R.attr.state_correct };
    private static final int[] STATE_INCORRECT = { R.attr.state_incorrect };

    private boolean isReset = true;
    private boolean isCorrect;

    public MultipleSelectionButton(Context context) {
        super(context);
    }

    public MultipleSelectionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultipleSelectionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] result;

        if (!isReset) {
            final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
            mergeDrawableStates(drawableState, isCorrect ? STATE_CORRECT : STATE_INCORRECT);
            result = drawableState;
        } else {
            result = super.onCreateDrawableState(extraSpace);
        }

        return result;
    }

    public final void feedback(boolean isCorrect) {
        this.isCorrect = isCorrect;
        this.isReset = false;
        refreshDrawableState();
    }

    public final void reset() {
        isReset = true;

        refreshDrawableState();
    }

    public boolean isReset() {
        return isReset;
    }

    public void setReset(boolean reset) {
        isReset = reset;
    }
}
