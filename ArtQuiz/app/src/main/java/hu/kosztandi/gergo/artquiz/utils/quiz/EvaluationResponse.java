package hu.kosztandi.gergo.artquiz.utils.quiz;

public enum EvaluationResponse {
    CORRECT,
    PARTLY_CORRECT,
    INCORRECT
}
