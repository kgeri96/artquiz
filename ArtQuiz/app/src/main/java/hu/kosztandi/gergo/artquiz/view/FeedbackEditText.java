package hu.kosztandi.gergo.artquiz.view;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatEditText;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResponse;

public class FeedbackEditText extends AppCompatEditText {
    private static final int[] STATE_CORRECT = { R.attr.state_correct };
    private static final int[] STATE_INCORRECT = { R.attr.state_incorrect };
    private static final int[] STATE_INACCURATE = { R.attr.state_inaccurate };

    private boolean isReset = true;
    private EvaluationResponse evaluationResponse;

    public FeedbackEditText(Context context) {
        super(context);
    }

    public FeedbackEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FeedbackEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] result;

        if (!isReset) {
            final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

            if (evaluationResponse == EvaluationResponse.CORRECT) {
                result = mergeDrawableStates(drawableState, STATE_CORRECT);
            } else if (evaluationResponse == EvaluationResponse.PARTLY_CORRECT) {
                result = mergeDrawableStates(drawableState, STATE_INACCURATE);
            } else {
                result = mergeDrawableStates(drawableState, STATE_INCORRECT);
            }
        } else {
            result = super.onCreateDrawableState(extraSpace);
        }

        return result;
    }

    public final void feedback(final EvaluationResponse response) {
        isReset = false;
        evaluationResponse = response;
        refreshDrawableState();
    }

    public final void reset() {
        isReset = true;
        refreshDrawableState();
    }
}
