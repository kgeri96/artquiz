package hu.kosztandi.gergo.artquiz.service;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import hu.kosztandi.gergo.artquiz.ArtQuiz;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;
import hu.kosztandi.gergo.artquiz.model.utilities.database.DBHelper;
import hu.kosztandi.gergo.artquiz.utils.EraSetMaker;

public class ArtPiecesService {
    private static final ArtPiecesService ourInstance = new ArtPiecesService();
    private ArrayList<String> allEras;
    private DBHelper dbHelper;

    public static ArtPiecesService getInstance() {
        return ourInstance;
    }

    private ArtPiecesService() {

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        dbHelper.close();
    }

    public final ArrayList<ArtPiece> getConfiguredShuffledArtPieceList() {
        ArrayList<ArtPiece> artPieces = getArtPiecesBySettings();

        if (artPieces.size() < QuizSettings.getInstance().getQuestionLimit()) {
            return new ArrayList<>();
        }

        Collections.shuffle(artPieces);

        return new ArrayList<>(artPieces.subList(0, Math.toIntExact(QuizSettings.getInstance().getQuestionLimit())));
    }

    public final ArrayList<ArtPiece> getArtPiecesForSelectedEras() {

        ArrayList<ArtPiece> artPieces = getArtPiecesBySettings();

        if(artPieces.size() < 1) {
            return new ArrayList<>();
        }

        return artPieces;
    }

    private ArrayList<ArtPiece> getArtPiecesBySettings() {
        checkDBHelper();
        Cursor artWorks = dbHelper.getArtPiecesBySettings();

        return populateArtpieceList(artWorks);
    }

    public final ArrayList<ArtPiece> getAllArtPieces() {
        checkDBHelper();
        Cursor artWorks = dbHelper.getArtPieces();

        return populateArtpieceList(artWorks);
    }

    public final ArrayList<String> getAllEras() {
        checkDBHelper();
        if (allEras == null) {
            allEras = new ArrayList<>();
            Cursor periods = dbHelper.getPossibleEras();

            for(periods.moveToFirst(); !periods.isAfterLast(); periods.moveToNext()) {
                allEras.add(periods.getString(0));
            }
        }

        return allEras;
    }

    public final ArrayList<ArtPiece> getArtPiecesFromSimilarEra(String era) {
        checkDBHelper();
        Set<String> similarEras = EraSetMaker.getEraSet(era);
        String[] similarErasArray = similarEras.toArray(new String[0]);

        return populateArtpieceList(dbHelper.getArtPiecesByEra(similarErasArray));
    }

    public final long getArtPieceCount() {
        checkDBHelper();

        return dbHelper.getArtPieceCount();
    }

    private ArrayList<ArtPiece> populateArtpieceList(Cursor artWorks) {
        ArrayList<ArtPiece> artPieces = new ArrayList<>();

        for(artWorks.moveToFirst(); !artWorks.isAfterLast(); artWorks.moveToNext()) {
            ArtPiece artPiece = new ArtPiece(artWorks.getInt(0), artWorks.getString(1), artWorks.getString(2), artWorks.getString(3));
            artPieces.add(artPiece);
        }

        artWorks.close();
        return artPieces;
    }

    public final long getArtPieceCount(ArrayList<String> selectedEras) {
        if (selectedEras.size() > 0) {
            checkDBHelper();
            return dbHelper.getArtPieceCount(selectedEras);
        } else {
            return 0;
        }
    }

    public final boolean insertArtpiece(ArtPiece artPiece) {
        checkDBHelper();

        return dbHelper.insertArtPiece(artPiece);
    }

    public final boolean deleteArtpiece(ArtPiece artPiece) {
        checkDBHelper();

        return dbHelper.deleteArtPiece(artPiece.getId());
    }

    private void checkDBHelper() {
        if (dbHelper == null) {
            dbHelper = new DBHelper(ArtQuiz.getContext());
        }
    }
}
