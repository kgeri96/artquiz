package hu.kosztandi.gergo.artquiz.common;

public interface FeedbackListener {
    void onCorrectAnswer();
    void onInaccurateAnswer(String mistake);
    void onWrongAnswer();
}
