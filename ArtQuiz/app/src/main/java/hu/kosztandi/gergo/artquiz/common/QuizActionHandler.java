package hu.kosztandi.gergo.artquiz.common;

import hu.kosztandi.gergo.artquiz.utils.quiz.UserAction;

public interface QuizActionHandler {
    void onUserAction(UserAction action);
}
