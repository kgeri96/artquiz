package hu.kosztandi.gergo.artquiz.common;

public interface PermissionCommand {
    void execute();
}
