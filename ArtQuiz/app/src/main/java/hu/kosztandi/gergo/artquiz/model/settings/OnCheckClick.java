package hu.kosztandi.gergo.artquiz.model.settings;

public interface OnCheckClick {
    void onClick(int position, boolean isChecked);
}
