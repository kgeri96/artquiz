package hu.kosztandi.gergo.artquiz.view.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.model.settings.OnCheckClick;
import hu.kosztandi.gergo.artquiz.model.settings.QuizSettings;

public class EraAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<String> dataSource;
    private boolean[] checkStatus;
    private OnCheckClick listener;

    public EraAdapter(Context context, ArrayList<String> dataSource, OnCheckClick listener) {
        this.context = context;
        this.dataSource = dataSource;
        this.listener = listener;
        checkStatus = new boolean[dataSource.size()];
        setChecks();
    }

    private void setChecks() {
        for(int i = 0; i < dataSource.size(); i++) {
            checkStatus[i] = QuizSettings.getInstance().getSelectedEras().contains(dataSource.get(i));
        }
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public Object getItem(int i) {
        return dataSource.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final int pos = position;
        final ViewHolder viewHolder;
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.era_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.era = convertView.findViewById(R.id.eraText);
            viewHolder.checkBox = convertView.findViewById(R.id.eraCheckBox);
            viewHolder.root = convertView.findViewById(R.id.rowRoot);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                checkStatus[pos] = isChecked;
                if(isChecked && pos == dataSource.size()-1) {
                    uncheckAllExceptLast();
                } else if(isChecked && pos != dataSource.size()-1) {
                    uncheckLast();
                }

                listener.onClick(pos, isChecked);
            }

            private void uncheckLast() {
                checkStatus[checkStatus.length-1] = false;
                notifyDataSetChanged();
            }

            private void uncheckAllExceptLast() {
                for(int i = 0; i < checkStatus.length-1; i++) {
                    checkStatus[i] = false;
                }
                notifyDataSetChanged();
            }
        });
        viewHolder.checkBox.setChecked(checkStatus[position]);
        viewHolder.era.setText(dataSource.get(position));


        return convertView;
    }

    private class ViewHolder {
        ViewGroup root;
        TextView era;
        CheckBox checkBox;
    }

    public boolean[] getCheckStatus() {
        return checkStatus;
    }

}
