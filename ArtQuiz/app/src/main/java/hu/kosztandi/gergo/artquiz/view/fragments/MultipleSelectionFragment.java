package hu.kosztandi.gergo.artquiz.view.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.common.Evaluator;
import hu.kosztandi.gergo.artquiz.model.AppConstants;
import hu.kosztandi.gergo.artquiz.model.ArtPiece;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResponse;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResult;
import hu.kosztandi.gergo.artquiz.utils.quiz.EvaluationResultFactory;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;
import hu.kosztandi.gergo.artquiz.utils.quiz.QuizType;
import hu.kosztandi.gergo.artquiz.view.MultipleSelectionButton;

public class MultipleSelectionFragment extends ImageQuiz {
    private boolean isEvaluated = false;

    private Activity context;

    private MultipleSelectionButton[] multipleSelectionButtons;
    private ImageButton nextButton;
    private MultipleSelectionButton selectedButton;
    private MultipleSelectionButton correctButton;
    private ViewGroup imageFragmentContainer;
    private ScrollView scrollView;

    public static QuizBase newInstance(final ArrayList<ArtPiece> artPieces, final QuizType quizType) {
        MultipleSelectionFragment fragment = new MultipleSelectionFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(ART_PIECE_LIST_KEY, artPieces);
        args.putInt(QUIZ_TYPE_KEY, quizType.ordinal());
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();

        final View view = inflater.inflate(R.layout.fragment_multiple_selection_quiz, container, false);

        nextButton = view.findViewById(R.id.next);
        imageFragmentContainer = view.findViewById(R.id.imageContainer);
        scrollView = view.findViewById(R.id.scrollView2);

        scrollView.setOnTouchListener((v, event) -> {
            scrollView.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });

        setNextButton();
        setMultipleSelectionButtons(view);
        handleMultipleSelectionButtonUpdate();

        return view;
    }

    private void setNextButton() {
        nextButton.setVisibility(View.GONE);
        nextButton.setOnClickListener(button -> {
            isEvaluated = false;
            nextButton.setVisibility(View.GONE);
            selectedButton.reset();
            correctButton.reset();
            next();
        });
    }

    @Override
    protected void updateUI() {
        clearSelection();
        handleMultipleSelectionButtonUpdate();
        nextButton.setVisibility(View.GONE);
        imageFragment.updateImage(artPieces.get(currentArtPieceIndex).getImageUrl());
    }

    private void clearSelection() {
        for (MultipleSelectionButton button : multipleSelectionButtons) {
            button.setTextColor(context.getColor(R.color.mainDark));
            ViewGroup highlight = (ViewGroup)button.getParent();
            highlight.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    protected void handleFeedback(EvaluationResult result) {
        if (result.response != EvaluationResponse.CORRECT) {
            selectedButton.feedback(false);
        }

        correctButton.feedback(true);
    }

    @Override
    protected Evaluator getQuizEvaluator() {
        return userAnswer -> EvaluationResultFactory.create(quizType, userAnswer, artPieces.get(currentArtPieceIndex).getName());
    }

    @Override
    protected ViewGroup getImageFragmentContainer() {
        return imageFragmentContainer;
    }

    @Override
    public int getCorrectAnswerValue() {
        return 1;
    }

    private void setMultipleSelectionButtons(final View view) {
        multipleSelectionButtons = new MultipleSelectionButton[AppConstants.multipleSelectionNumber];

        for (int i = 0; i < multipleSelectionButtons.length; i++) {
            multipleSelectionButtons[i] = view.findViewWithTag("option_"+i);
        }

        for (Button multipleSelectionButton : multipleSelectionButtons) {
            multipleSelectionButton.setOnClickListener(new MSButtonClickListener());
        }
    }

    private void handleMultipleSelectionButtonUpdate() {
        String currentArtPieceEra = artPieces.get(currentArtPieceIndex).getEra();
        String[] possibleAnswers = getRandomUniqueAnswersFromList(ArtPiecesService.getInstance().getArtPiecesFromSimilarEra(currentArtPieceEra));
        boolean hasCorrectAnswer = false;

        for (int i = 0; i < possibleAnswers.length; i++) {
            multipleSelectionButtons[i].setText(possibleAnswers[i]);
            if (possibleAnswers[i].equals(artPieces.get(currentArtPieceIndex).getName())) {
                hasCorrectAnswer = true;
                setCorrectButton(i);
            }
        }

        if (!hasCorrectAnswer) {
            int random = new Random().nextInt(multipleSelectionButtons.length);
            multipleSelectionButtons[random].setText(artPieces.get(currentArtPieceIndex).getName());
            setCorrectButton(random);
        }
    }

    private void setCorrectButton(int buttonIndex) {
        correctButton = multipleSelectionButtons[buttonIndex];
    }

    private String[] getRandomUniqueAnswersFromList(ArrayList<ArtPiece> artPieceList) {
        String[] answers = new String[AppConstants.multipleSelectionNumber];
        int[] randoms;

        int tryCount = 0;
        do {
            randoms = new Random()
                    .ints(100, 0, artPieceList.size())
                    .distinct()
                    .limit(AppConstants.multipleSelectionNumber)
                    .toArray();
            tryCount++;
        } while (checkDuplicates(randoms, artPieceList) && tryCount < 50);

        for (int i = 0; i < randoms.length; i++) {
            answers[i] = artPieceList.get(randoms[i]).getName();
        }

        if (checkDuplicates(randoms, artPieceList)) {
            for (int i = 0; i < AppConstants.multipleSelectionNumber / 2; i++) {
                changeDuplicates(answers);
            }
        }

        return answers;
    }

    private void changeDuplicates(final String[] answers) {
        final List<Integer> changedIndices = new ArrayList<>();
        final List<String> changedAnswers = new ArrayList<>();

        for (int i = 0; i < answers.length && !changedIndices.contains(i); i++) {
            for (int j = 0; j < answers.length && !changedIndices.contains(j); j++) {
                if (i != j && answers[i].equals(answers[j])) {
                    if (new Random().nextBoolean()) {
                        answers[i] = getOtherAnswer(changedAnswers, answers[i], answers);
                        changedIndices.add(i);
                    } else {
                        answers[j] = getOtherAnswer(changedAnswers, answers[j], answers);
                        changedIndices.add(j);
                    }
                }
            }
        }
    }

    private String getOtherAnswer(final List<String> changedAnswers, final String currentAnswer, final String[] answers) {
        final List<String> otherAnswers =
                ArtPiecesService.getInstance()
                        .getAllArtPieces()
                        .stream()
                        .filter(artPiece -> !artPiece.getName().equals(currentAnswer) && !changedAnswers.contains(artPiece.getName()) && !Arrays.asList(answers).contains(artPiece.getName()))
                        .map(ArtPiece::getName)
                        .collect(Collectors.toList());
        final String randomOther = otherAnswers.get(new Random().nextInt(otherAnswers.size()));

        changedAnswers.add(randomOther);

        return randomOther;
    }

    private boolean checkDuplicates(int[] randoms, ArrayList<ArtPiece> artPieceList) {
        boolean hasDuplicate = false;

        for (int i = 0; i < randoms.length; i++) {
            for (int j = 0; j < randoms.length; j++) {
                if (i != j && artPieceList.get(randoms[i]).getName().equals(artPieceList.get(randoms[j]).getName())) {
                    hasDuplicate = true;
                    break;
                }
            }
        }

        return hasDuplicate;
    }

    private class MSButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(final View view) {
            if (!isEvaluated && view instanceof MultipleSelectionButton) {
                MultipleSelectionButton clicked = (MultipleSelectionButton)view;
                selectedButton = clicked;
                evaluate(clicked.getText().toString());

                nextButton.setVisibility(View.VISIBLE);

                isEvaluated = true;
            }
        }
    }
}
