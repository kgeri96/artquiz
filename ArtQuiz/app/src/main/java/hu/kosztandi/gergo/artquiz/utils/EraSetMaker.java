package hu.kosztandi.gergo.artquiz.utils;

import java.util.ArrayList;
import java.util.HashSet;

import hu.kosztandi.gergo.artquiz.ArtQuiz;
import hu.kosztandi.gergo.artquiz.R;
import hu.kosztandi.gergo.artquiz.service.ArtPiecesService;

public class EraSetMaker {
    private static ArrayList<String> eras = ArtPiecesService.getInstance().getAllEras();
    private static final String[] presetEras = ArtQuiz.getContext().getResources().getStringArray(R.array.eras);

    public static HashSet<String> getEraSet(String era) {
        HashSet<String> similarEras;

        if(isPresetEra(era)) {
            similarEras = new HashSet<>();
            similarEras.add(era);
            for(int i = 0; i < presetEras.length; i++) {
                if(era.equals(presetEras[i])) {
                    putEras(similarEras, i);
                }
            }
            return similarEras;
        } else {
            return new HashSet<>(eras);
        }
    }

    /*
     * Korszakok:
     * 0 - Őskor
     * 1 - Ókori Egyiptom
     * 2 - Mezopotámia
     * 3 - Görögország
     * 4 - Róma
     * 5 - Korakeresztény
     * 6 - Népvándorlás kora
     * 7 - Bizánc
     * 8 - Romanika
     * 9 - Gótika
     * 10 - Reneszánsz
     * 11 - Barokk
     * 12 - Klasszicizmus
     * 13 - Romantika
     * 14 - Realizmus
     * 15 - Impresszionizmus
     * 16 - Neoimpresszionizmus
     * 17 - Posztimpresszionizmus
     * 18 - Szecesszió
     * 19 - Szimbolizmus
     * 20 - XX. század első fele
     * 21 - XX. század második fele
     * 22 - Fotó
     * 23 - Európán kívüli kultúrák
     * 24 - Eklektika
     */
    private static void putEras(HashSet<String> similarEras, int index) {
        switch (index) {
            //Őskor
            case 0:
                similarEras.add(presetEras[2]); //Mezopotámia
                similarEras.add(presetEras[1]); //Ókori Egyiptom
                break;
            //Ókori Egyiptom
            case 1:
                similarEras.add(presetEras[2]); //Mezopotámia
                break;
            //Mezopotámia
            case 2:
                similarEras.add(presetEras[1]); //Ókori Egyiptom
                similarEras.add(presetEras[6]); //Népvándorlás
                break;
            //Görögország
            case 3:
                similarEras.add(presetEras[4]); //Róma
                break;
            //Róma
            case 4:
                similarEras.add(presetEras[3]); //Görögország
                break;
            //Ókereszténység
            case 5:
                similarEras.add(presetEras[7]); //Bizánc
                break;
            //Népvándorlás kora
            case 6:
                similarEras.add(presetEras[2]); //Mezopotámia
                break;
            //Bizánc
            case 7:
                similarEras.add(presetEras[8]); //Romantika
                similarEras.add(presetEras[9]); //Gótika
                break;
            //Romanika
            case 8:
                similarEras.add(presetEras[9]); //Gótika
                break;
            //Gótika
            case 9:
                similarEras.add(presetEras[8]); //Romanika
                similarEras.add(presetEras[7]); //Bizánc
                break;
            //Reneszánsz
            case 10:
                similarEras.add(presetEras[11]); //Barokk
                similarEras.add(presetEras[12]); //Klasszicizmus
                similarEras.add(presetEras[3]); //Görögország
                break;
            //Barokk
            case 11:
                similarEras.add(presetEras[10]); //Reneszánsz
                break;
            //Klasszicizmus
            case 12:
                similarEras.add(presetEras[10]); //Reneszánsz
                break;
            //Romantika
            case 13:
                similarEras.add(presetEras[9]); //Gótika
                similarEras.add(presetEras[11]); //Barokk
                similarEras.add(presetEras[14]); //Realizmus
                break;
            //Realizmus
            case 14:
                similarEras.add(presetEras[13]); //Romantika
                break;
            //Impresszionizmus
            case 15:
                similarEras.add(presetEras[16]); //Neoimpresszionizmus
                similarEras.add(presetEras[17]); //Posztimpresszionizmus
                break;
            //Neo
            case 16:
                similarEras.add(presetEras[15]); //Impresszionizmus
                similarEras.add(presetEras[17]); //Posztimpresszionizmus
                break;
            //Poszt
            case 17:
                similarEras.add(presetEras[15]); //Impresszionizmus
                similarEras.add(presetEras[16]); //Neoimpresszionizmus
                break;
            case 18:
                similarEras.add(presetEras[19]); //Szimbolizmus
                similarEras.add(presetEras[24]); //Eklektika
                break;
            case 19:
                similarEras.add(presetEras[18]); //Szecesszió
                similarEras.add(presetEras[24]); //Eklektika
                break;
            case 20:
                similarEras.add(presetEras[21]); //XX. század második fele
                similarEras.add(presetEras[22]); //Fotó
                break;
            case 21:
                similarEras.add(presetEras[20]); //XX. század első fele
                similarEras.add(presetEras[22]); //Fotó
                break;
            case 22:
                similarEras.add(presetEras[20]); //XX. század első fele
                similarEras.add(presetEras[21]); //XX. század második fele
                break;
            case 23:
                similarEras.add(presetEras[6]); //Népvándorlás kora
                similarEras.add(presetEras[2]); //Mezopotámia
                break;
            case 24:
                similarEras.add(presetEras[18]); //Szecesszió
                similarEras.add(presetEras[19]); //Szimbolizmus
                break;
        }
    }

    private static boolean isPresetEra(String era) {
        for (String presetEra : presetEras) {
            if (presetEra.equals(era)) {
                return true;
            }
        }
        return false;
    }
}
